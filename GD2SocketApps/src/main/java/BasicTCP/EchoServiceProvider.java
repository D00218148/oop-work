package BasicTCP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class EchoServiceProvider
{
    public static void main(String[] args)
    {
        try
        {
            //step 1: set up a connection socket for other programs to connect to
            ServerSocket listeningSocket = new ServerSocket(EchoServiceDetails.LISTENING_PORT);

            boolean continueRunning = true;

            while(continueRunning)
            {
                //step 2: wait for incoming connections
                Socket dataSocket = listeningSocket.accept();

                //step 3: build the input and output objects
                OutputStream outputStream = dataSocket.getOutputStream();
                PrintWriter output = new PrintWriter(new OutputStreamWriter(outputStream));

                InputStream inputStream = dataSocket.getInputStream();
                Scanner input = new Scanner(new InputStreamReader(inputStream));

                String incomingMessage = "";
                while(!incomingMessage.equals("."))
                {
                    //step 4: exchange messages
                    incomingMessage = input.nextLine();

                    //prints incoming message
                    System.out.println(incomingMessage);

                    output.println(incomingMessage);
                    output.flush();
                }
                dataSocket.close();
            }
            listeningSocket.close();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
