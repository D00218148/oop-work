package extendsThread;

public class PrintChar extends Thread
{
    private char charToPrint;
    private int times;

    public PrintChar(char c, int t)
    {
        charToPrint = c;
        times = t;
    }

    //Override the run method to tell the thread what to do
    @Override
    public void run()
    {
        for(int i = 0; i < times; i++)
        {
            System.out.print(charToPrint);
        }
    }
}
