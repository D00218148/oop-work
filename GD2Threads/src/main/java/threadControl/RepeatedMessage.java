package threadControl;

public class RepeatedMessage implements Runnable
{
    private String message;
    private int pauseTime;

    //field stopFlag tells the thread when to stop
    //declared volatile because i want every thread to see
    //the change immediately - no caching

    private volatile boolean stopFlag;

    public RepeatedMessage(String message, int pauseTime)
    {
        this.message = message;
        this.pauseTime = pauseTime;
    }

    public synchronized static void displayMessage(RepeatedMessage rm) throws InterruptedException
    {
        for(int i = 0; i < rm.message.length(); i++)
        {
            System.out.print(rm.message.charAt(i));
            Thread.currentThread().sleep(50);
        }
        System.out.println();
    }

    @Override
    public void run()
    {
        stopFlag = false;
        try
        {
            while(!stopFlag)
            {
                displayMessage(this);
                Thread.currentThread().sleep(pauseTime);
            }
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void finish()
    {
        stopFlag = true;
        return;
    }
}
