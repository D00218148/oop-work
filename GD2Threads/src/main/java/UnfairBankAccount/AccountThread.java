package UnfairBankAccount;

public class AccountThread implements Runnable
{
    private Account acc;

    public AccountThread(Account acc)
    {
        this.acc = acc;
    }

    @Override
    public void run()
    {
        for(int i = 0; i < 10; i++)
        {
            acc.deposit(100);
        }
    }
}
