package com.dkit.gd2.antanaszalisauskas;

import java.util.Map;

public class ChuckNorrisJokes
{
    private String type;
    private Map<String, Object> value;

    public String getType() {
        return type;
    }

    public Map<String, Object> getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ChuckNorrisJokes{" +
                "type='" + type + '\'' +
                ", value=" + value +
                '}';
    }
}
