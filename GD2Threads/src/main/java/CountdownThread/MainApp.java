package CountdownThread;

public class MainApp
{
    public static void main(String[] args)
    {
        Countdown count = new Countdown();

        CountdownThread t1 = new CountdownThread(count);
        CountdownThread t2 = new CountdownThread(count);

        t1.setName("Thread 1");
        t2.setName("Thread 2");

        t1.start();
        t2.start();
    }
}
