package Client;

import Core.VotingSystemServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class VotingSystemClient
{
    public static void main(String[] args)
    {
        Socket dataSocket = null;
        try
        {
            //establish communication
            dataSocket = new Socket(VotingSystemServiceDetails.HOSTNAME, VotingSystemServiceDetails.LISTENING_PORT);

            //build input and output stream
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out), true);

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            boolean loggedIn = false;

            while(!loggedIn)
            {
                loggedIn = login(keyboard, input, output);
            }
            while(loggedIn && !message.equals(VotingSystemServiceDetails.EXIT))
            {
                message = getChoice(keyboard);
                output.println(message);
                String response = input.nextLine();
                System.out.println(response);
            }
            System.out.println("Thank you for using the vote client");
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Error closing socket");
                System.exit(1);
            }
        }
    }

    private static String getChoice(Scanner keyboard)
    {
        String option = "";
        String vote = "";
        System.out.println("Options are 'Vote' or 'Exit'");
        option = keyboard.nextLine();
        if(option.equalsIgnoreCase("Vote"))
        {
            System.out.println("Options are 'Yes', 'No', 'Dunno'");
            vote = keyboard.nextLine();
            return VotingSystemServiceDetails.VOTE + VotingSystemServiceDetails.BREAKING_CHARACTERS + vote;
        }
        else if(option.equalsIgnoreCase("Exit"))
        {
            return VotingSystemServiceDetails.EXIT;
        }
        else
        {
            return getChoice(keyboard);
        }
    }

    private static boolean login(Scanner keyboard, Scanner input, PrintWriter output)
    {
        String username;
        String password;
        String message;
        String response;

        System.out.println("Please enter your username: ");
        username = keyboard.nextLine();
        System.out.println("Please enter your password: ");
        password = keyboard.nextLine();

        //send login request to server
        message = VotingSystemServiceDetails.LOGIN + VotingSystemServiceDetails.BREAKING_CHARACTERS + username + VotingSystemServiceDetails.BREAKING_CHARACTERS + password;

        output.println(message);

        //get server response
        response = input.nextLine();
        if(response.equals(VotingSystemServiceDetails.SUCCESSFUL_LOGIN))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
