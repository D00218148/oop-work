
package com.dkit.gd2.antanaszalisauskas;



/**
 *
 * @author 
 */
public class Permanent extends Staff
{
    private int overnightAllowance;

    public Permanent(String id, String name, int bonus, String baseAirport, double baseAirport1, int overnightAllowance) {
        super(id, name, bonus, baseAirport, baseAirport1);
        this.overnightAllowance = overnightAllowance;
    }

    public int getOvernightAllowance() {
        return overnightAllowance;
    }

    @Override
    public String getBaseAirport() {
        return baseAirport;
    }

    @Override
    public double getBaseAirport1() {
        return baseAirport1;
    }

    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getBonus() {
        return super.getBonus();
    }

    @Override
    public String toString() {
        return super.toString() + ", overnight allowance=" + overnightAllowance;
    }

    public void updateBonus(Flight flight)
    {
        if(!this.baseAirport.equals(flight.getDestination()))
        {
            this.bonus = this.bonus + this.overnightAllowance;
        }
    }
}
