/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas.gd2solarsystem;
//hw - read collections chapter, finish main by adding in planets & moons
//do exercises sent on tuesday

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author D00218148
 */
public class Main 
{
    private static Map<String, HeavenlyBody> solarSystem = new HashMap<>();
    private static Set<HeavenlyBody> planets = new HashSet<>();
    
    public static void main(String[] args) {
        HeavenlyBody mercury = new HeavenlyBody("Mercury", 88);
        HeavenlyBody venus = new HeavenlyBody("Venus", 224.7);
        HeavenlyBody earth = new HeavenlyBody("Earth", 365.2);
        HeavenlyBody mars = new HeavenlyBody("Mars", 687);
        HeavenlyBody jupiter = new HeavenlyBody("Jupiter", 4380);
        HeavenlyBody saturn = new HeavenlyBody("Saturn", 10585);
        HeavenlyBody uranus = new HeavenlyBody("Uranus", 30660);
        HeavenlyBody neptune = new HeavenlyBody("Neptune", 60225);
        HeavenlyBody temp = new HeavenlyBody("Pluto", 90520);
        
        solarSystem.put(mercury.getName(), mercury);
        planets.add(mercury);
        
        solarSystem.put(venus.getName(), venus);
        planets.add(venus);
        
        solarSystem.put(earth.getName(), earth);
        planets.add(earth);
        
        HeavenlyBody tempMoon = new HeavenlyBody("Moon", 27.3);
        earth.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(mars.getName(), mars);
        planets.add(mars);
        
        tempMoon = new HeavenlyBody("Phobos", 0.333);
        mars.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Deimos", 1.25);
        mars.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(jupiter.getName(), jupiter);
        planets.add(jupiter);

        tempMoon = new HeavenlyBody("Io", 1.75);
        jupiter.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Europa", 3.541);
        jupiter.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Ganymede", 7.166);
        jupiter.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Callisto", 17);
        jupiter.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(saturn.getName(), saturn);
        planets.add(saturn);

        tempMoon = new HeavenlyBody("Mimas", 0.9);
        saturn.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Enceladus", 1.4);
        saturn.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Tethys", 1.9);
        saturn.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Dione", 2.7);
        saturn.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(uranus.getName(), uranus);
        planets.add(uranus);

        tempMoon = new HeavenlyBody("Cordelia", 0.335);
        uranus.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Ophelia", 0.376);
        uranus.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Bianca", 0.434);
        uranus.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Cressida", 0.463);
        uranus.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(neptune.getName(), neptune);
        planets.add(neptune);

        tempMoon = new HeavenlyBody("Naiad", 0.294);
        neptune.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Thalassa", 0.311);
        neptune.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Despina", 0.334);
        neptune.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Galatea", 0.428);
        neptune.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        solarSystem.put(temp.getName(), temp);
        planets.add(temp);

        tempMoon = new HeavenlyBody("Charon", 6.375);
        temp.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Nix", 25);
        temp.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Hydra", 38);
        temp.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        tempMoon = new HeavenlyBody("Kerberos", 32);
        temp.addMoon(tempMoon);
        solarSystem.put(tempMoon.getName(), tempMoon);
        
        System.out.println("Planets");
        for(HeavenlyBody planet: planets)
        {
            System.out.println("\t" + planet.getName()); //things in a hashmap are not ordered
        }
        
        //print all moons of mars
        System.out.println("Mars moons");
        for(HeavenlyBody marsMoon : mars.getSatellites())
        {
            System.out.println("\t" + marsMoon.getName());
        }
        
        //print all moons in solar system
        Set<HeavenlyBody> moons = new HashSet<>();
        for(HeavenlyBody planet: planets)
        {
            moons.addAll(planet.getSatellites()); //adds all moons into new set
        }
        
        //printing moons
        System.out.println("All moons");
        for(HeavenlyBody moon: moons)
        {
            System.out.println("\t" + moon.getName());
        }
        
        HeavenlyBody pluto = new HeavenlyBody("Pluto", 842);
        planets.add(pluto); //adds a second pluto in a SET
        //default equals in objects compares addresses therefore its added
        for(HeavenlyBody planet: planets)
        {
            System.out.println(planet.getName() + ": " + planet.getOrbitalPeriod());
        }
        
        
    }
}
