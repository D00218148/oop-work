package Server;

import Core.LottoServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class LottoServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;
    private static final int MAX_GENERATE_NUMBERS = 7;

    public LottoServiceThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMesage = "";
        String response;

        try
        {
            while(!incomingMesage.equals(LottoServiceDetails.CLOSE))
            {
                response = null;
                //take input from client
                incomingMesage = input.nextLine();
                System.out.println("Recieved message: " + incomingMesage);

                //break message to components
                String[] components = incomingMesage.split(LottoServiceDetails.COMMAND_SEPARATOR);

                if(components[0].equals(LottoServiceDetails.GENERATE))
                {
                    if(components.length > 1)
                    {
                        response = generateResponse(Integer.parseInt(components[1]));
                    }
                }
                else
                {
                    response = LottoServiceDetails.UNRECOGNISED;
                }

                System.out.println("Sending response: " + response);
                output.println(response);
            }
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            System.out.println("closing connection with client #" + number);
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Oopsie");
                System.exit(1);
            }
        }
    }

    public static String generateResponse(int numberOfNumbers)

    {
        if(numberOfNumbers > MAX_GENERATE_NUMBERS)
        {
            return "Number is too big";
        }

        StringBuilder message = new StringBuilder(LottoServiceDetails.GENERATED);
        message.append(LottoServiceDetails.COMMAND_SEPARATOR);
        Random random = new Random();
        int[] nums = random.ints(1, 49).distinct().limit(numberOfNumbers).sorted().toArray();
        for(int i = 0; i < numberOfNumbers; i++)
        {
            message.append(Integer.toString(nums[i]));
            message.append(LottoServiceDetails.COMMAND_SEPARATOR);
        }
        return message.toString();
    }
}
