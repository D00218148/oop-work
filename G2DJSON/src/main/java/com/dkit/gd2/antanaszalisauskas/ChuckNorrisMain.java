package com.dkit.gd2.antanaszalisauskas;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ChuckNorrisMain
{
    public static void main(String[] args)
    {
        final String CHUCK_NORRIS_JOKES_URL = "http://api.icndb.com/jokes/random";

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            ChuckNorrisJokes jokes = mapper.readValue(new URL(CHUCK_NORRIS_JOKES_URL), ChuckNorrisJokes.class);
            System.out.println(jokes.getValue().get("joke"));
        }
        catch(MalformedURLException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonParseException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
