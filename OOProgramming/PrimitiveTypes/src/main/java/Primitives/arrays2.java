/*
Write method called readIntegers(count), with count repping how many integers you entered.
needs to read from the console until all numbers are entered, and return an array of those numbers.

write a method called findMin() which takes an array as a parameter and returns the min value.

in the main read the count of the numbers and call readIntegers(count), then call findMin(array) and finally print the min number
 */
package Primitives;


public class arrays2 
{
    public static void main(String[] args) 
    {
        int[] numbers = {7,9,23,543,7,5,457};
        
        System.out.println("Minimum of array: " + findMin(numbers));
    }
    
    private static int findMin(int[] numbers)
    {
        int min = numbers[0];
        for(int i : numbers)
        {
            if(min > i)
            {
                min = i;
            }
        }
        return min;
    }
}
