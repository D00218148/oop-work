/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas.gd2solarsystem;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/*
Challenge - when overriding the equals method in the heavenlybody
class we were careful to make sure that it would not return true is
a heavenlybody was compared to a subclass of itself
This was unnecessary in heavenlybody why? heavenlybody cannot be subclassed
as it is FINAL
*/

/**
 *
 * @author D00218148
 */
public final class HeavenlyBody 
{
    private final String name;
    private final double orbitalPeriod;
    private final Set<HeavenlyBody> satellites;

    public HeavenlyBody(String name, double orbitalPeriod) {
        this.name = name;
        this.orbitalPeriod = orbitalPeriod;
        this.satellites = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public double getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public Set<HeavenlyBody> getSatellites() {
        return new HashSet<HeavenlyBody>(this.satellites);
    }
    
    public boolean addMoon(HeavenlyBody moon)
    {
        return this.satellites.add(moon);
    }

    @Override //to override method name and parameters must be same
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        System.out.println("object class: " + obj.getClass());
        System.out.println("this class: " + this.getClass());
        if((obj == null) || (obj.getClass() != this.getClass()))
        {
            return false;
        }
        String objName = ((HeavenlyBody)obj).getName();
        return this.name.equals(objName);
    }
    
    @Override
    public int hashCode()
    {
        System.out.println("Hashcode called");
        return this.name.hashCode() + 57;
    }
    
}
