package Tutorial;

public class Counter
{
    int count = 0;

    public Counter() {
        this.count = 0;
    }

    public void increment()
    {
        count += 1;
    }

    public int getCount() {
        return count;
    }
}
