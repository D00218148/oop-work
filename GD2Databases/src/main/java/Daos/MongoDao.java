/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import Exceptions.DaoException;
import com.mongodb.MongoException;
import com.mongodb.MongoClient;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/*
 *
 * @author D00218148
 */

public class MongoDao 
{
    private MongoClient connection = null;

    public MongoClient getConnection()
    {
        //default will connect to localhost port 27017
        try
        {
            connection = new MongoClient();
            System.out.println("Connected to local Mongo Server");

        }
        catch(MongoException e)
        {
            System.out.println(e.getMessage());
        }
        return connection;

    }

    
    public void freeConnection(MongoClient con)
    {
        try
        {
            if(con != null)
            {
                con.close();
                con = null;
            }
        }
        catch(MongoException e)
        {
            System.out.println("Failed to free connection" + e.getMessage());
            System.exit(1);
        }
    }
    
}
