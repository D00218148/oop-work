package BasicTCP;

import java.io.*;
import java.net.Inet4Address;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class EchoServiceConsumer
{
    public static void main(String[] args)
    {
        try
        {
            //step 1: establish a channel of communication
            Socket dataSocket = new Socket(Inet4Address.getByName("10.102.224.166"), EchoServiceDetails.LISTENING_PORT);

            //step 2: build the output and input objects
            OutputStream outputStream = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(outputStream));

            InputStream inputStream = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(inputStream));

            Scanner sc = new Scanner(System.in);
            String message = "";
            while(!message.equals("."))
            {
                System.out.println("Please enter a message");
                message = sc.nextLine();

                output.println(message);
                output.flush();

                String response = input.nextLine();
                System.out.println("Response: " + response);
            }
            dataSocket.close();
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
