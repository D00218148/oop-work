package com.dkit.gd2.antanaszalisauskas;

//use map to store qs and as
//get random question and tell whether user is right or wrong

import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;


public class MapDemo
{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<String, String> qna = new HashMap<String, String>();

        qna.put("How many shrimps did i eat?", "11");
        qna.put("How yellow is the sky?", "not very");
        qna.put("Hotel?", "trivago");
        qna.put("How high?", "how little");


        randomQuestion(qna);
        System.out.println("Enter your answer");
        String input = sc.nextLine();

        /*
        if(checkAnswer(qna, input))
        {
            System.out.println("Your are correct");
        }
        else
        {
            System.out.println("mmmmmmm try again");
        }

        /*
        Map<String, String> languages = new HashMap<>();
        languages.put("Java", "I don't know what to say about this");
        languages.put("Javascript", "Like Java, only worser");
        languages.put("C++", "thing");
        languages.put("PHP", "idk");
        languages.put("Python", "kk");
        languages.put("MySQL", "it exists");

        if(languages.replace("MySQL", "it exists", "xd"))
        {
            System.out.println("Replaced");
        }
        else
        {
            System.out.println("Not replaced");
        }

        languages.remove("PHP");

        if(languages.remove("Python", "kk"))
        {
            System.out.println("Python removed");
        }
        else
        {
            System.out.println("Python not removed");
        }

        if(languages.containsKey("Java"))
        {
            System.out.println("Java already exists");
        }
        else
        {
            languages.put("Java", "coolio");
        }

        System.out.println(languages.put("Java", "coolio")); //previous value for key
        System.out.println(languages.put("Haskell", "it kinda exists")); // null(didnt have any previous value)

        printMap(languages);
        */
    }
    public static String randomQuestion(Map<String, String> questions)
    {
        Random generator = new Random();
        Object[] values = questions.keySet().toArray();
        Object randomValue = values[generator.nextInt(values.length)];
        return (String)randomValue;
    }

    public static void printMap(Map<String, String> mapToPrint)
    {
        for(String key: mapToPrint.keySet())
        {
            System.out.println(key + ": " + mapToPrint.get(key));
        }
    }
}
