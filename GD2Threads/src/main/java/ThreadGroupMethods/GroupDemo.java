package ThreadGroupMethods;

import java.io.IOException;

public class GroupDemo implements Runnable
{
    public static void main(String[] args)
    {
        //create a thread group
        ThreadGroup parent = new ThreadGroup("parent");

        //create a group which is a child of another group
        ThreadGroup subgroup = new ThreadGroup(parent, "subgroup");

        //create some threads in the parent and subgroup
        Thread t1 = new Thread(parent, new GroupDemo());
        Thread t2 = new Thread(parent, new GroupDemo());
        Thread t3 = new Thread(subgroup, new GroupDemo());

        t1.start();
        t2.start();
        t3.start();

        parent.list();

        //wait for user input to exit
        System.out.println("Press enter to continue");
        try
        {
            System.in.read();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        System.exit(0);
    }

    @Override
    public void run()
    {
        //do nothing
        for(;;)
        {
            //pause the thread so another can execute
            Thread.yield();
        }
    }
}
