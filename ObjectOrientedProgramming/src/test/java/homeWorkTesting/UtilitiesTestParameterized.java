package homeWorkTesting;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class UtilitiesTestParameterized
{
    private Utilities util;
    private String input;
    private String output;

    @org.junit.Before
    public void setup()
    {
        util = new Utilities();
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testConditions()
    {
        return Arrays.asList(new Object[][]{
        {"ABCDEFF", "ABCDEF"},
        {"ABBCDDEFF", "ABCDEF"},
        {"AAABCDDDEFF", "ABCDEF"},
        {"A", "A"}
    });
    }

    @org.junit.Test
    public void removePairs()
    {
        assertEquals(output, util.removePairs(input));
    }
}
