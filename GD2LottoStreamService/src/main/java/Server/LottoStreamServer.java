package Server;

import Core.LottoServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLOutput;

public class LottoStreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        try
        {
            //set up listening socket
            listeningSocket = new ServerSocket(LottoServiceDetails.LISTENING_PORT);

            //set up a threadGroup to manage all clients together
            ThreadGroup clientGroup = new ThreadGroup("Client threads");

            clientGroup.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //do main logic of server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning)
            {
                //Accept incoming connections
                dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("The server has now accepted " + threadCount + " clients");

                LottoServiceThread newClient = new LottoServiceThread(clientGroup, dataSocket.getInetAddress()+"", dataSocket, threadCount);
                newClient.start();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("Oopsie");
                System.exit(1);
            }
        }
    }
}
