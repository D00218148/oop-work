package PetRegistrySystem;
//Author: Antanas Zalisauskas(D00218148)

import java.io.Serializable;
import java.util.Comparator;

public class Pet implements Comparable<Pet>, Serializable
{
    private String dateRegistered;
    private String petID;
    private String ownerID;
    private String animalType;
    private long serialVersionUID = 1L;

    public Pet(String dateRegistered, String petID, String ownerID, String animalType)
    {
        this.dateRegistered = dateRegistered;
        this.petID = petID;
        this.ownerID = ownerID;
        this.animalType = animalType;
    }

    public String getDateRegistered() {
        return dateRegistered;
    }

    public String getPetID() {
        return petID;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public String getAnimalType() {
        return animalType;
    }

    public String toString()
    {
        return dateRegistered + " " + petID + " " + ownerID + " " + animalType;
    }

    protected void SetOwnerID(String ownerID)
    {
        this.ownerID = ownerID;
    }

    @Override
    public int compareTo(Pet pet) {
        return this.dateRegistered.compareTo(pet.dateRegistered) * -1;
    }
}
