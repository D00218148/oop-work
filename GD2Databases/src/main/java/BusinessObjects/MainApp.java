package BusinessObjects;

import DTOs.User;
import Daos.IUserDaoInterface;
import Daos.MongoUserDAO;
import Daos.MySQLUserDAO;
import Daos.MySqlDao;
import Exceptions.DaoException;
import java.util.List;
import java.util.logging.Level;

public class MainApp
{
    public static void main(String[] args)
    {
        java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
        IUserDaoInterface IUserDao = new MongoUserDAO();
        /*
        notice that the userDao reference is an Interface type
        this will allow for different concrete implementations
        we could change to Mongo without changing anything in
        the interface
        if the interface doesn't change, none of the code below needs
        to change
        the contract defined by the interface will not be broken
        this means that this code is independent of the code used
        to access the database. (reduced coupling)
        the business objects will require that all user DAOs implement
        the IUserDAOInterface as the code uses only references to
        the interface
        */
      
        try
        {
            List<User> users = IUserDao.findAllUsers();
            if(users.isEmpty())
            {
                System.out.println("There are no users");
            }
            for(User user: users)
            {
                System.out.println("User: " + user.toString());
            }
            
            //test dao with good username and password
            User user = IUserDao.findUserByUsernamePassword("burnem", "password");
            if(user != null)
            {
                System.out.println(user.toString());
            }
            else
            {
                System.out.println("No user with that username and password found");
            }
            
            //test dao with bad username and password
            User user2 = IUserDao.findUserByUsernamePassword("bunem", "password");
            if(user2 != null)
            {
                System.out.println(user.toString());
            }
            else
            {
                System.out.println("No user with that username and password found");
            }
        }
        catch(DaoException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
