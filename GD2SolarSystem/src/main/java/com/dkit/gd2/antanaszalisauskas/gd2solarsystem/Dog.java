/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas.gd2solarsystem;

/**
 *
 * @author D00218148
 */
public class Dog 
{
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public final boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        if(obj instanceof Dog)
        {
            String objName = ((Dog)obj).getName();
            return this.name.equals(objName);
        }
        return false;
    }
}
