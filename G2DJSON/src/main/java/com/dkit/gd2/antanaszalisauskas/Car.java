package com.dkit.gd2.antanaszalisauskas;

public class Car
{
    private String body;
    private int wheels;
    private int doors;
    private String model;
    private String make;
    private int topSpeed;
    private String fuelType;
    private double engineSize;
    private int noughtToSixty;
    private String colour;

    public Car(String body, int wheels, int doors, String model, String make, int topSpeed, String fuelType, double engineSize, int noughtToSixty, String colour) {
        this.body = body;
        this.wheels = wheels;
        this.doors = doors;
        this.model = model;
        this.make = make;
        this.topSpeed = topSpeed;
        this.fuelType = fuelType;
        this.engineSize = engineSize;
        this.noughtToSixty = noughtToSixty;
        this.colour = colour;
    }

    public Car() {
    }

    @Override
    public String toString() {
        return "Car{" +
                "body='" + body + '\'' +
                ", wheels=" + wheels +
                ", doors=" + doors +
                ", model='" + model + '\'' +
                ", make='" + make + '\'' +
                ", topSpeed=" + topSpeed +
                ", fuelType='" + fuelType + '\'' +
                ", engineSize=" + engineSize +
                ", noughtToSixty=" + noughtToSixty +
                ", colour='" + colour + '\'' +
                '}';
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getBody() {
        return body;
    }

    public int getWheels() {
        return wheels;
    }

    public int getDoors() {
        return doors;
    }

    public String getModel() {
        return model;
    }

    public String getMake() {
        return make;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public String getFuelType() {
        return fuelType;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public int getNoughtToSixty() {
        return noughtToSixty;
    }

    public String getColour() {
        return colour;
    }
}
