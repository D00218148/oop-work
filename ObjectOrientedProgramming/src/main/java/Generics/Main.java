package Generics;

import java.util.ArrayList;
import java.util.Collections;

public class Main
{
    public static void main(String[] args)
    {
        /*
        ArrayList<Integer> items = new ArrayList<>();
        items.add(1);
        items.add(2);
        items.add(3);
        items.add(4);
        items.add(5);

        printDoubled(items);
         */

        //create a generic class to implement a league table for a sport.
        //the class should allow teams to be added to the list and store
        //a list of teams that belong to the league
        //your class should have method to print out the teams in order,
        //with the team at the top of the league printed first
        //only teams of the same type should be added to a particular instance of
        //league class - the program should fail to compile if an attempt is made to add an incompatible team

        SoccerPlayer gamer = new SoccerPlayer("Gamer");
        BasketballPlayer gamerer = new BasketballPlayer("Gamerer");
        HurlingPlayer gamererer = new HurlingPlayer("gamererer");

        Team<SoccerPlayer> dundalkFC = new Team<>("Dundalk FC");
        dundalkFC.addPlayer(gamer);

        Team<BasketballPlayer> dundalkBC = new Team<>("Dundalk BC");
        dundalkBC.addPlayer(gamerer);

        Team<HurlingPlayer> dundalkHC = new Team<>("Dundalk HC");
        dundalkHC.addPlayer(gamererer);

        Team<SoccerPlayer> droghedaFC = new Team<>("Drogheda FC");
        Team<SoccerPlayer> fakeTeam = new Team<>("Fake Team");

        fakeTeam.matchResult(dundalkFC, 10, 0);
        fakeTeam.matchResult(droghedaFC, 10, 0);
        System.out.println(dundalkFC.getWins() + " wins");
        System.out.println(droghedaFC.getLosses() + " losses");

        System.out.println(dundalkFC.ranking());
        System.out.println(droghedaFC.ranking());

        System.out.println(dundalkFC.compareTo(droghedaFC));

        ArrayList<Team> teams = new ArrayList<>();
        teams.add(dundalkFC);
        teams.add(droghedaFC);
        teams.add(fakeTeam);
        Collections.sort(teams);
        printTeams(teams);

        League<Team<SoccerPlayer>> soccerLeague = new League("Bundesliga");
        soccerLeague.add(dundalkFC);
        soccerLeague.add(droghedaFC);
        soccerLeague.add(fakeTeam);
    }

    private static void printTeams(ArrayList<Team> teams)
    {
        for(Team i: teams)
        {
            System.out.println(i.getName());
        }
    }

    public static void printDoubled(ArrayList<Integer> items)
    {
        for(int i: items)
        {
            System.out.println(i*2);
        }
    }
}
