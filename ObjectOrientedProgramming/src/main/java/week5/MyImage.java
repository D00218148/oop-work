package week5;

public class MyImage
{
    private String name;

    public MyImage(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
