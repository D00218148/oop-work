/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;

import java.util.ArrayList;
/**
 *
 * @author D00218148
 */
public class Fish extends Animal
{
    private int gills;
    private int eyes;
    private int fins;

    public Fish(int gills, int eyes, int fins, String name, int age, String habitat, boolean wild, int size, int weight, int dangerLevel, int averageLifespan, ArrayList<String> colour) {
        super(name, age, habitat, wild, size, weight, dangerLevel, averageLifespan, colour);
        this.gills = gills;
        this.eyes = eyes;
        this.fins = fins;
    }
    
    private void moveFins()
    {
        System.out.println("Fish.swim() called");
    }
    
    private void rest()
    {
        System.out.println("Fish.rest() called");
    }

    private void swim(int speed)
    {
        moveFins();
        move(speed);
    }
    
    @Override
    public String toString() {
        return "Fish{" + "gills=" + gills + ", eyes=" + eyes + ", fins=" + fins + super.toString() + '}';
    }
    
    
}
