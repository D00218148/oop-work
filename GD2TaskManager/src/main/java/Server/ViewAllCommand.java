package Server;

import Core.Task;
import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.util.Date;
import java.util.Set;

public class ViewAllCommand implements Command
{
    @Override
    public String generateResponse(String[] components, TaskManager taskList) {
        //StringBuilder response = new StringBuilder();
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_VIEW)
        {
            Set<Task> tasks = taskList.getAllTasks();
            response = TaskManagerServiceDetails.flattenTaskSet(tasks);
            if(response == null)
            {
                response = "Dummy task%%Dummy Owner%%" + new Date().getTime();
            }
        }
        return response;
    }
}
