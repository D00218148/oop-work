package Client;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboServiceConsumer
{
    public static void displayMenu()
    {
        System.out.println("Please enter one of the following options: ");
        System.out.println("1) Echo a message");
        System.out.println("2) Get current date an time");
        System.out.println("3) Get number of requests to the server");
        System.out.println("Enter -1 to end program");
    }

    public static int getChoice(Scanner input)
    {
        int choice = 0;
        boolean validNumber = false;
        while(!validNumber)
        {
            try
            {
                choice = input.nextInt();
                validNumber = true;
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number from the menu");
                System.out.println();
                input.nextLine();
                displayMenu();
            }
        }
        return choice;
    }

    public static void main(String[] args) {
        //set up scanner to get input from user
        Scanner input = new Scanner(System.in);

        //Set up socket for sending and receiving
        DatagramSocket requestorSocket = null;

        try
        {
            //set up address info of the server
            InetAddress serviceProviderHost = InetAddress.getByName("localhost");

            //set up datagram socket for sending and receiving
            requestorSocket = new DatagramSocket(ComboServiceDetails.requestorListeningPort);

            boolean continueRunning = true;

            while(continueRunning)
            {
               displayMenu();

               int choice = getChoice(input);
               String message = null;
               boolean sendMessage = true;

               switch(choice)
               {
                   case -1:
                       continueRunning = false;
                       sendMessage = false;
                       break;
                   case 1:
                       input.nextLine();
                       System.out.println("What message do you want to echo: ");
                       message = "echo"+ComboServiceDetails.breakingCharacters+input.nextLine();
                       break;
                   case 2:
                       message = "daytime";
                       break;
                   case 3:
                       message = "stats";
                       break;
                   default:
                       System.out.println("Bad request. Please choose from the menu of options");
                       sendMessage = false;
               }
               if(sendMessage)
               {
                   //process the message to send
                   byte buffer[] = message.getBytes();

                   //build datagram packet
                   //buffer - byte array to send
                   //length of buffer
                   //serviceProviderHost - ip address of server
                   //providerListeningPort - server port number
                   DatagramPacket requestPacket = new DatagramPacket(buffer, buffer.length, serviceProviderHost, ComboServiceDetails.providerListeningPort);

                   //send the datagram
                   requestorSocket.send(requestPacket);
                   System.out.println("Message sent");

                   //receive the response
                   //Create a buffer
                   byte[] responseBuffer = new byte[ComboServiceDetails.MAX_LEN];
                   DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length);

                   //wait to receive a response
                   requestorSocket.receive(responsePacket);
                   //get data out of packet
                   String data = new String(responsePacket.getData());
                   System.out.println("Response: " + data.trim() + ".");
               }
                System.out.println();
            }
            System.out.println("Thank you for using the Combo Service program");
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(requestorSocket != null)
            {
                requestorSocket.close();
            }
        }
    }
}
