/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week6;

/**
 *
 * @author D00218148
 */
public class PlayerMain 
{
    public static void main(String[] args) 
    {
        Player player = new Player();
        player.fullName = "Bob";
        player.hitPoints = 100;
        player.weapon = "Longsword";
        
        int damage = 10;
        player.loseHealth(damage);
        System.out.println(player.healthRemaining());
        
        damage = 11;
        player.loseHealth(damage);
        System.out.println(player.healthRemaining());
        //----------------------------------------------
        EncapsulatedPlayer player2 = new EncapsulatedPlayer("Bobby", -100, "Sword");
        System.out.println(player2.getHitPoints());
    }
}
