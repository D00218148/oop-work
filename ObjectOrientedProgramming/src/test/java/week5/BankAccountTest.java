package week5;

import static org.junit.Assert.*;

public class BankAccountTest {
    private BankAccount testAccount;

    @org.junit.Before
    public void setUp() throws Exception
    {
        BankAccount testAccount = new BankAccount("123", 100.00, "James", "james@gmail.coom", "+353-1234567");
    }

    @org.junit.Test
    public void depositFifty()
    {
        testAccount.deposit(50);
        assertEquals(150.00, testAccount.getBalance(), 0.0000001);
    }

    @org.junit.Test
    public void depositZero()
    {
        testAccount.deposit(0);
        assertEquals(100.00, testAccount.getBalance(), 0.0000001);
    }

    @org.junit.Test
    public void depositMinusTen()
    {
        testAccount.deposit(-10);
        assertEquals(100.00, testAccount.getBalance(), 0.0000001);
    }

    @org.junit.Test
    public void withdrawZero()
    {
        testAccount.withdraw(0);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }

    @org.junit.Test
    public void withdrawMax()
    {
        testAccount.withdraw(100.00);
        assertEquals(0, testAccount.getBalance(), 0.00000001);
    }

    @org.junit.Test
    public void withdrawMinusTen()
    {
        testAccount.withdraw(-10);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }

    @org.junit.Test
    public void withdrawOneOOne()
    {
        testAccount.withdraw(101.00);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }
}