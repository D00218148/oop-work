/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.Scanner;
/**
 *
 * @author D00218148
 */
public class arrays4 
{
    private static Scanner keyboard = new Scanner(System.in);
    private static int[] baseData = new int[10];
    
    public static void main(String[] args) 
    {
        System.out.println("Enter 10 integers");
        getInput();
        printArray(baseData);
        resizeArray();
        System.out.println("Enter 12 integers");
        printArray(baseData);
        
    }
    
    private static void getInput()
    {
        for(int i = 0; i < baseData.length; i++)
        {
            baseData[i] = keyboard.nextInt();
        }
    }
    
    private static void printArray(int[] array)
    {
        for(int i = 0; i < array.length; i++)
        {
            System.out.println(array[i] + " ");
        }
    }
    
    private static void resizeArray()
    {
        int[] original = baseData;
        baseData = new int[12];
        
        for(int i = 0; i < original.length; i++)
        {
            baseData[i] = original[i];
        }
    }
}
