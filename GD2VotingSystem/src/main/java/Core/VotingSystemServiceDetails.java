package Core;

public class VotingSystemServiceDetails
{
    public static final String BREAKING_CHARACTERS = "%";
    public static final String VOTE_REJECTED_SEPARATOR = "%%";

    public static final String HOSTNAME = "localhost";
    public static final int LISTENING_PORT = 22517;

    public static final String LOGIN = "login";
    public static final String SUCCESSFUL_LOGIN = "SUCCESSFUL";
    public static final String FAILED_LOGIN = "FAILED";

    public static final String VOTE = "vote";
    public static final String ACCEPTED_VOTE = "ACCEPTED";
    public static final String REJECTED_VOTE = "REJECTED";

    public static final String EXIT = "exit";
    public static final String GOODBYE = "GOODBYE";
}
