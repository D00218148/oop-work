package week7;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main
{
    private static Scanner sc = new Scanner(System.in);
    private static Locations locations = new Locations();

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        Map<String, String> vocabulary = new HashMap<String, String>();
        vocabulary.put("REGISTER", "R");
        vocabulary.put("LOGIN", "L");
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");

        int loc = 1;

        Map<String, String> loginDetails = RegisterUser();
        while(true)
        {
            if(loginUser(loginDetails))
            {
                System.out.println(locations.get(loc).getDescription());
                if(loc == 0)
                {
                    break;
                }

                Map<String, Integer> exits = locations.get(loc).getExits();
                System.out.println("Available exits are: ");
                for(String exit : exits.keySet())
                {
                    System.out.print(exit + ", ");
                }
                System.out.println();

                String direction = sc.nextLine().toUpperCase();
                if(direction.length() > 1)
                {
                    String[] words = direction.split(" ");
                    for(String word : words)
                    {
                        if(vocabulary.containsKey(word))
                        {
                            direction = vocabulary.get(word);
                            break;
                        }
                    }
                }

                if(exits.containsKey(direction))
                {
                    loc = exits.get(direction);
                }
                else{
                    System.out.println("You cannot go that direction");
                }
            }
            else
            {
                break;
            }

        }

    }

    private static Map RegisterUser()
    {
        Map<String, String> loginDetails = new HashMap<String, String>();
        System.out.println("Enter username");
        String username = sc.nextLine();
        System.out.println("Enter password");
        String password = sc.nextLine();
        sc.nextLine();
        loginDetails.put(username,password);

        return loginDetails;
    }

    private static boolean loginUser(Map<String, String> loginDetails)
    {
        System.out.println("Enter username");
        String username = sc.nextLine();
        System.out.println("Enter password");
        String password = sc.nextLine();
        sc.nextLine();

        if(loginDetails.containsKey(username) && loginDetails.containsValue(password))
        {
            return true;
        }
        else
        {
            System.out.println("Invalid details");
            return false;
        }
    }
}
