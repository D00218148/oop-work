/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2AutoBoxChallenge;
import java.util.ArrayList;
/**
 *
 * @author D00218208
 */
public class Bank 
{
    private ArrayList<Branch> branches = new ArrayList<Branch>();
    
    public void addBranch(Branch branch)
    {
        branches.add(branch);
        System.out.println("Branch Added");
    }
    
    public void addCustomerToBranch(Customer customer)
    {
        branches.add(Branch.addCustomer(customer));
    }
    
    public void addTransactionToCustomer(Branch branch, double transaction)
    {
        branch.getCustomer().addTransaction(transaction);
    }
    
    public void listOfCustomersInBranch(Branch branch)
    {
        branch.customerList();
    }
}
