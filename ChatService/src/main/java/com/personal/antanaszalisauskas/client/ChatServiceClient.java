package com.personal.antanaszalisauskas.client;

import com.personal.antanaszalisauskas.core.ChatServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ChatServiceClient
{
    public static void main(String[] args) {
        try
        {
            //Step 1: establish a connection with the server
            //Like a phone call, first thing you do is
            //dial the number you want to talk to
            Socket dataSocket = new Socket("localhost", ChatServiceDetails.LISTENING_PORT);

            //Step 2 build output and input streams
            OutputStream out = dataSocket.getOutputStream();
            //wrapped in printWriter for efficiency
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            System.out.println("Welcome. Please enter choice(!QUIT, !MESSAGE, !HELP)");

            while(!message.equals(ChatServiceDetails.END_SESSION))
            {
                String choice = getChoice(keyboard);
                String response = "";

                if (ChatServiceDetails.END_SESSION.equals(choice))
                {
                    message = ChatServiceDetails.END_SESSION;

                    //send message
                    output.println(message);
                    output.flush();

                    response = input.nextLine();
                    if (response.equals(ChatServiceDetails.SESSION_TERMINATED)) {
                        System.out.println("Session ended");
                    }
                }
                else if (ChatServiceDetails.HELP_COMMANDS.equals(choice))
                {
                    message = ChatServiceDetails.HELP_COMMANDS;

                    //send message
                    output.println(message);
                    output.flush();

                    //get response
                    response = input.nextLine();

                    System.out.println("received response: " + response);
                }
                else if (ChatServiceDetails.MESSAGE.equals(choice))
                {
                    message = generateMessage(keyboard);

                    //send message
                    output.println(message);
                    output.flush();

                    response = input.nextLine();
                    System.out.println(response);
                }
                /*if(response.equals(ComboServiceDetails.URECOGNISED))
                {
                    System.out.println("not recognised");
                }

                 */
            }

            System.out.println("Thank you for using our thing");
            dataSocket.close();
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static String getChoice(Scanner keyboard)
    {
        String choice = "";
        while(choice.equals(""))
        {
            try
            {
                choice = keyboard.nextLine();
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number: ");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return choice;
    }

    public static String generateMessage(Scanner keyboard)
    {
        //setup the command text
        StringBuffer message = new StringBuffer(ChatServiceDetails.MESSAGE);
        message.append(ChatServiceDetails.COMMAND_SEPARATOR);
        //get the message to be echoed
        System.out.println("Enter your message");
        String echo = keyboard.nextLine();
        message.append(echo);

        return message.toString();
    }
}
