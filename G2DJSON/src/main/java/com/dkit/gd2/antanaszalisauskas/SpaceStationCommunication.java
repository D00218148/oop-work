package com.dkit.gd2.antanaszalisauskas;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class SpaceStationCommunication
{
    public static void main(String[] args)
    {
        final String ISS_NOW_URI = "http://api.open-notify.org/iss-now.json";

        try
        {
            ObjectMapper mapper = new ObjectMapper();
            Location ssLocation = mapper.readValue(new URL(ISS_NOW_URI), Location.class);
            System.out.println(ssLocation);
            System.out.println(ssLocation.getIss_position().get("latitude"));
        }
        catch(MalformedURLException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonParseException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
