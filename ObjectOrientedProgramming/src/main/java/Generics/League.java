package Generics;

import java.util.ArrayList;
import java.util.Collections;

public class League<T extends Team>
{
    private String name;
    private ArrayList<T> teams = new ArrayList<>();

    public League(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean add(T team)
    {
        if(teams.contains(team))
        {
            System.out.println("Team already in league");
            return false;
        }
        else
        {
            teams.add(team);
            System.out.println("Team added");
            return true;
        }
    }
    
    public void showLeagueTable()
    {
        Collections.sort(teams);
        for(T team: teams)
        {
            System.out.println(team.getName() + ": " + team.ranking());
        }
    }

}
