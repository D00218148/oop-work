package Server;

public interface Command
{
    public String createResponse(String[] components);
}
