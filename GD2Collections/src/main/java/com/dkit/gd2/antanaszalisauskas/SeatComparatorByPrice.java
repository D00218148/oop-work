package com.dkit.gd2.antanaszalisauskas;

import java.util.Comparator;

public class SeatComparatorByPrice implements Comparator<Theatre.Seat>
{
    @Override
    public int compare(Theatre.Seat seat1, Theatre.Seat seat2)
    {
        if(seat1.getPrice() < seat2.getPrice())
        {
            return -1;
        }
        else if(seat1.getPrice() == seat2.getPrice())
        {
            return 0;
        }
        return 1;
    }
}
