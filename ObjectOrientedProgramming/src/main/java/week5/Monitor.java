package week5;

public class Monitor
{
    private String model;
    private String manufacturer;
    private int size;
    private Resolution nativeResolution;
    private int refreshRate;
    private String screenType;

    public Monitor(String model, String manufacturer, int size, Resolution nativeResolution, int refreshRate, String screenType) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.size = size;
        this.nativeResolution = nativeResolution;
        this.refreshRate = refreshRate;
        this.screenType = screenType;
    }

    public void drawPixelAt(int x, int y, String colour)
    {
        System.out.println("Drawing pixel at " + x + "," + y + " in colour " + colour);
    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getSize() {
        return size;
    }

    public Resolution getNativeResolution() {
        return nativeResolution;
    }

    public int getRefreshRate() {
        return refreshRate;
    }

    public String getScreenType() {
        return screenType;
    }
}

