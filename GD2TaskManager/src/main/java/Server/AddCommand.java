package Server;

import Core.Task;
import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.util.Date;

public class AddCommand implements Command
{
    @Override
    public String generateResponse(String[] components, TaskManager taskList) {
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_ADD)
        {
            try
            {
                String taskName = components[TaskManagerServiceDetails.TASK_NAME_INDEX];
                String taskOwner = components[TaskManagerServiceDetails.TASK_OWNER_INDEX];
                long deadline = Long.parseLong(components[TaskManagerServiceDetails.TASK_DATE_INDEX]);

                Task newTask = new Task(taskName, taskOwner, new Date(deadline));
                boolean added = taskList.add(newTask);
                if(added)
                {
                    response = TaskManagerServiceDetails.SUCCESSFUL_ADD;
                }
                else
                {
                    response = TaskManagerServiceDetails.FAILED_ADD;
                }
            }
            catch(NumberFormatException e)
            {
                response = TaskManagerServiceDetails.FAILED_ADD;
                System.out.println(e.getMessage());
            }
        }
        return response;
    }
}
