package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ComboStreamServer
{
    public static void main(String[] args)
    {
        try
        {
            //setup listening socket
            ServerSocket listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);

            //setup thread group to manage all client threads
            ThreadGroup clientThreadGroup = new ThreadGroup("Client threads");

            //place more emphasis on accepting clients than processing them
            //by setting their priority to be one less than the main thread
            clientThreadGroup.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //do the main logic of server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning)
            {
                //wait for incoming connection and build communication link
                Socket dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("The server has now accepted " + threadCount + " clients");

                //build the thread
                //need to give thread a few things:
                //1. group it needs to be stored in
                //2. name for thread
                //3. socket to communicate through
                //4. any other info
                ComboServiceThread newClient = new ComboServiceThread(clientThreadGroup, dataSocket.getInetAddress()+"", dataSocket, threadCount);
                newClient.start();
            }
            listeningSocket.close();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
