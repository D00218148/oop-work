package Interfaces;

public class MobilePhone implements ITelephone
{
    private int number;
    private boolean isRinging;
    private boolean isOn = false;

    public MobilePhone(int number)
    {
        this.number = number;
        this.isRinging = false;
    }

    @Override
    public void powerOn()
    {
        System.out.println("Turning on phone");
        isOn = true;
    }

    @Override
    public void dial(int phoneNumber)
    {
        if(isOn)
        {
            System.out.println("Dialing " + phoneNumber);
        }
    }

    @Override
    public void answer()
    {
        if(isOn && isRinging)
        {
            System.out.println("Answering phone");
            isRinging = false;
        }
    }

    @Override
    public boolean isRinging() {
        return isRinging;
    }

    public void setRinging(boolean ringing) {
        isRinging = ringing;
    }
}
