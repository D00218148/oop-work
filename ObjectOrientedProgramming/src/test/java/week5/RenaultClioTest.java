package week5;

import org.junit.Test;

import static org.junit.Assert.*;

public class RenaultClioTest
{
    @Test
    public void accelerateSpeed()
    {
        RenaultClio brokenCar = new RenaultClio(36);
        brokenCar.accelerate(100);
        assertEquals(100, brokenCar.getCurrentSpeed());
    }

    @Test
    public void accelerateGear()
    {
        RenaultClio brokenCar = new RenaultClio(36);
        brokenCar.accelerate(100);
        assertEquals("4", brokenCar.getCurrentGear());
    }
}