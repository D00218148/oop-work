/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homeWorkTesting;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author D00218148
 */
public class UtilitiesTest {
    
    public UtilitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of everyNthChar method, of class Utilities.
     */
    @Test
    public void testEveryNthChar() {
        char[] sourceArray = {'h','e','l','l','o'};
        int n = 2;
        Utilities instance = new Utilities();
        char[] expResult = new char[]{'e','l'};
        char[] result = instance.everyNthChar(sourceArray, n);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void EveryNthCharNSmaller() {
        char[] sourceArray = {'h','e','l','l','o'};
        int n = 2;
        Utilities instance = new Utilities();
        char[] expResult = new char[]{'h','e','l','l','o'};
        char[] result = instance.everyNthChar(sourceArray, n);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void EveryNthCharNBigger() {
        char[] sourceArray = {'h','e','l','l','o'};
        int n = 6;
        Utilities instance = new Utilities();
        char[] expResult = new char[]{'h','e','l','l','o'};
        char[] result = instance.everyNthChar(sourceArray, n);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of removePairs method, of class Utilities.
     */
    
    @Test
    public void testRemovePairsTest1() {
        System.out.println("removePairsTest1");
        String source = "AABCDDEEFF";
        Utilities util = new Utilities();
        String expResult = "ABCDEF";
        String result = util.removePairs(source);
        assertEquals(expResult, result);
    }

    @Test
    public void testRemovePairsTest2() {
        System.out.println("removePairsTest2");
        String source = "ABCCABDEEF";
        Utilities util = new Utilities();
        String expResult = "ABCABDEF";
        String result = util.removePairs(source);
        assertEquals(expResult, result);
    }

    @Test
    public void testRemovePairsTest3() {
        System.out.println("removePairsTest3");
        String source = null;
        Utilities util = new Utilities();
        String expResult = null;
        String result = util.removePairs(source);
        assertNull(expResult, result);
    }

    /**
     * Test of converter method, of class Utilities.
     */
    @Test
    public void testConverter() {
        int a = 10;
        int b = 5;
        Utilities instance = new Utilities();
        int expResult = 300;
        int result = instance.converter(a, b);
        assertEquals(expResult, result);
    }

    @Test(expected = ArithmeticException.class)
    public void testConverterException() {
        int a = 10;
        int b = 0;
        Utilities instance = new Utilities();
        int expResult = 300;
        int result = instance.converter(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of nullIfOddLength method, of class Utilities.
     */
    @Test
    public void testNullIfOddLength() {
        String source = "odd";
        Utilities instance = new Utilities();
        String expResult = null;
        String result = instance.nullIfOddLength(source);
        assertNull(expResult, result);
    }

    @Test
    public void testNullIfOddLengthEven() {
        String source = "even";
        Utilities instance = new Utilities();
        String expResult = "even";
        String result = instance.nullIfOddLength(source);
        assertNotNull(expResult, result);
    }
    
}
