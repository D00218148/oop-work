/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;
/**
 *
 * @author D00218148
 */
public class BankAccount 
{
    private String firstName;
    private String lastName;
    private double balance;
    private int typeOfAccount;
    
    private static final int CHECKING = 1;
    private static final int SAVINGS = 2;
    
    public BankAccount(String firstName, String lastName, double balance, int typeOfAccount)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.balance = balance;
        this.typeOfAccount = typeOfAccount;
    }
    
    public double deposit(double amount, boolean branch)
    {
        balance += amount;
        return balance;
    }
    
    public double withdraw(double amount, boolean branch)
    {
        if(amount > 500 && branch == false)
        {
            throw new IllegalArgumentException();
        }
        balance -= amount;
        return balance;
    }
    
    public double getBalance()
    {
        return balance;
    }
    
    public boolean isChecking()
    {
        return typeOfAccount == CHECKING;
    }
}
