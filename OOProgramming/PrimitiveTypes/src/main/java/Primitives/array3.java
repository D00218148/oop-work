/*
write a method called reverse() that takes an int array as parameter
the method should not return a value but it is allowed to alter the array passed to it
in main test reverse function
 */
package Primitives;

import java.util.Arrays;

/**
 *
 * @author D00218148
 */
public class array3 
{
    public static void main(String[] args) 
    {
        int[] normalArray = {1,2,3,4,5,6,7};
        System.out.println(Arrays.toString(normalArray));
        
        reverse(normalArray);
        
        System.out.println("reversed array: " + Arrays.toString(normalArray));
    }
    
    private static void reverse(int[] normalArray)
    {
        int maxIndex = normalArray.length - 1;
        int halfLength = normalArray.length/2;
        
        for(int i = 0; i < halfLength; i++)
        {
            int temp = normalArray[i];
            normalArray[i] = normalArray[maxIndex - i];
            normalArray[maxIndex - i] = temp;
        }
    }
}
