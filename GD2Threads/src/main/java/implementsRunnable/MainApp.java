package implementsRunnable;

import extendsThread.PrintChar;
import extendsThread.PrintNum;

public class MainApp
{
    public static void main(String[] args)
    {
        //implements runnable requires you to wrap in Thread class
        Thread printA = new Thread(new PrintChar('A', 100));
        Thread printB = new Thread(new PrintChar('B', 100));
        Thread print100 = new Thread(new PrintNum(100));

        //you must start() for the thread to run
        print100.start();
        printA.start();
        printB.start();
    }
}
