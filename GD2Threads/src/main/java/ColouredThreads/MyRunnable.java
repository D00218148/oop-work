package ColouredThreads;

import static ColouredThreads.ThreadColour.ANSI_RED;

public class MyRunnable implements Runnable
{
    @Override
    public void run()
    {
        System.out.println(ANSI_RED + "Hell from MyRunnable's run()");
    }
}
