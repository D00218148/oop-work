package Tutorial;

public class CounterThread implements Runnable
{
    Counter count;

    public CounterThread(Counter count) {
        this.count = count;
    }

    @Override
    public void run()
    {
        count.increment();
    }
}
