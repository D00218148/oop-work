package week6;

//import com.sun.scenario.effect.impl.sw.java.JSWBlend_SRC_OUTPeer;
import org.w3c.dom.ls.LSOutput;

public class Closet
{
    private int length;
    private int width;
    private int height;
    private String doorType;
    private int doors;
    private boolean hasMirror;

    public Closet(int length, int width, int height, String doorType, int doors, boolean hasMirror) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.doorType = doorType;
        this.doors = doors;
        this.hasMirror = hasMirror;
    }

    public void openCloset()
    {
        System.out.println("Opening closet");
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getDoorType() {
        return doorType;
    }

    public int getDoors() {
        return doors;
    }

    public boolean isHasMirror() {
        return hasMirror;
    }
}
