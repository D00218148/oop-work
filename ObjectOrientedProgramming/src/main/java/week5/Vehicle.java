/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;

import java.util.ArrayList;

/**
 *
 * @author D00218148
 */
public class Vehicle 
{
    private String name;
    private String size;
    private String terrain;
    private int maxSpeed;
    private int currentSpeed;
    private int currentDirection;
    private ArrayList<String> colour;

    public Vehicle(String name, String size, String terrain, int maxSpeed, ArrayList<String> colour) {
        this.name = name;
        this.size = size;
        this.terrain = terrain;
        this.maxSpeed = maxSpeed;
        this.currentSpeed = 0;
        this.currentDirection = 0;
        this.colour = colour;
    }
    
    public void steer(int direction)
    {
        this.currentDirection += direction;
        System.out.println("Vehicle.steer(): steering at " + this.currentDirection + " degrees");
    }
    
    public void move(int speed, int direction)
    {
        this.currentSpeed = speed;
        this.currentDirection = direction;
        System.out.println("Vehicle.move(): moving at " + this.currentSpeed + " int direction " + this.currentDirection);
    }

    public void stop()
    {
        this.currentSpeed = 0;
    }
    
    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public String getTerrain() {
        return terrain;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }

    public ArrayList<String> getColour() {
        return colour;
    }
    
}
