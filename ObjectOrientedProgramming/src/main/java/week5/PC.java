package week5;

public class PC
{
    private Motherboard motherboard;
    private Monitor monitor;
    private Case pcCase;
    private GPU gpu;

    public PC(Motherboard motherboard, Monitor monitor, Case pcCase, GPU gpu)
    {
        this.motherboard = motherboard;
        this.monitor = monitor;
        this.pcCase = pcCase;
        this.gpu = gpu;
    }

    public Motherboard getMotherboard()
    {
        return motherboard;
    }

    public Monitor getMonitor()
    {
        return monitor;
    }

    public Case getPcCase()
    {
        return pcCase;
    }

    public GPU getGpu()
    {
        return gpu;
    }
}
