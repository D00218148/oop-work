package PetRegistrySystem;
//Author: Antanas Zalisauskas(D00218148)

import java.util.Comparator;

public class Fish extends Pet
{
    private String type;
    private String name;
    private String breed;
    private int age;
    private String colour;
    private String gender;
    private String waterType;

    public Fish(String dateRegistered, String petID, String ownerID, String animalType, String type, String name, String breed, int age, String colour, String gender, String waterType) {
        super(dateRegistered, petID, ownerID, animalType);
        this.type = type;
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.colour = colour;
        this.gender = gender;
        this.waterType = waterType;
    }

    public String getAnimalType()
    {
        return super.getAnimalType();
    }

    public String getPetID()
    {
        return super.getPetID();
    }

    public String getDateRegistered()
    {
        return super.getDateRegistered();
    }

    public String getOwnerID()
    {
        return super.getOwnerID();
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public int getAge() {
        return age;
    }

    public String getColour() {
        return colour;
    }

    public String getGender() {
        return gender;
    }

    public String getWaterType() {
        return waterType;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWaterType(String waterType) {
        this.waterType = waterType;
    }

    /*
     * comparePets, when called takes in an instance of Fish and compares
     * the current instance(this) to the other instance. If all variables are the same
     * the method returns true meaning both Fish instances are the same
     * @param tempPet - the other instance that is being compared to
     */
    public boolean comparePets(Fish tempPet)
    {
        if((this.getType().equals(tempPet.getType())) && (this.getName().equals(tempPet.getName())) && (this.getBreed().equals(tempPet.getBreed())) &&
                (this.getAge() == tempPet.getAge()) && (this.getColour().equals(tempPet.getColour())) && (this.getGender().equals(tempPet.getGender())) && (this.getWaterType().equals(tempPet.getWaterType())))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /*
     * checkID is used for whenever the user wants to edit pet details, or remove a pet.
     * This method takes in a String id which is compared to current instance of Fish.
     * If id matches, method returns true
     * @param id - user inputted id which is compared
     */
    public boolean checkID(String id)
    {
        if(this.getPetID().equals(id))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public String toString()
    {
        return super.toString() + " " + this.type + " " + this.name + " " + this.breed + " " + this.age + " " + this.colour + " " + this.gender + " " + this.waterType;
    }

    public void SetOwnerID(String ownerID)
    {
        super.SetOwnerID(ownerID);
    }

    /*
     * GenderComparator and AgeComparator are used to sort fish ArrayList by gender or age
     */
    public static Comparator<Fish> GenderComparator = new Comparator<Fish>()
    {
        @Override
        public int compare(Fish pet1, Fish pet2)
        {
            return pet1.getGender().compareTo(pet2.getGender());
        }
    };

    public static Comparator<Fish> AgeComparator = new Comparator<Fish>()
    {
        @Override
        public int compare(Fish pet1, Fish pet2)
        {
            return pet1.getAge() - pet2.getAge();
        }
    };
}
