package week9;

import java.util.Arrays;

public class ObjectSorting
{
    public static void main(String[] args)
    {
        int[] intArr = {5, 9, 1, 10};
        Arrays.sort(intArr);
        System.out.println(Arrays.toString(intArr));

        Employee[] employeeArray = new Employee[4];
        employeeArray[0] = new Employee(10, "Garry", 22, "Sales", 20000);
        employeeArray[1] = new Employee(20, "Brian", 19, "Crew Member", 15000);
        employeeArray[2] = new Employee(5, "Manny", 23, "Counsellor", 40000);
        employeeArray[3] = new Employee(1, "Aleksandr", 20, "Cat Sitter", 15000);

        Arrays.sort(employeeArray, Employee.NameComparator);
        System.out.println(Arrays.toString(employeeArray));
        Arrays.sort(employeeArray, Employee.AgeComparator);
        System.out.println(Arrays.toString(employeeArray));

        Arrays.sort(employeeArray, new EmployeeComparatorByIdAndName());
        System.out.println(Arrays.toString(employeeArray));
    }
}
