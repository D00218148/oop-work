package ReaderWriter;

public class Message
{
    private String message;
    private boolean isEmpty = true;

    public synchronized String read()
    {
        while(isEmpty)
        {
            try
            {
                wait();
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }
        isEmpty = true;
        notifyAll();
        return message;
    }

    public synchronized void write(String message)
    {
        while(!isEmpty)
        {
            try
            {
                wait();
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }
        isEmpty = false;
        this.message = message;
        notifyAll();
    }
}
