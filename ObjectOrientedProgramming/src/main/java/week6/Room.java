package week6;

public class Room
{
    private String roomName;
    private int walls;
    private int length;
    private int width;
    private int windows;
    private Bed bed;
    private Drawer drawer;
    private Closet closet;
    private Lamp lamp;
}
