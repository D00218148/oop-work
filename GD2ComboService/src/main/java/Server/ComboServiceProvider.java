package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.*;
import java.util.Date;

public class ComboServiceProvider
{
    public static void main(String[] args)
    {
        //variable to track the number of requests
        int countRequests = 0;
        boolean continueRunning = true;

        DatagramSocket providerSocket = null;

        try
        {
            //create socket to send and receive messages
            providerSocket = new DatagramSocket(ComboServiceDetails.providerListeningPort);

            System.out.println("Listening on port: " + ComboServiceDetails.providerListeningPort);

            //loop forever and process requests
            while(continueRunning)
            {
                //create a buffer to hold the input message
                byte[] inncomingMessageBuffer = new byte[ComboServiceDetails.MAX_LEN];
                DatagramPacket incomingPacket = new DatagramPacket(inncomingMessageBuffer, inncomingMessageBuffer.length);

                //wait to receive a message
                //this is blocking
                providerSocket.receive(incomingPacket);
                //get data out of packet
                String data = new String(incomingPacket.getData());

                //process the data, trim off any excess white space
                data = data.trim();

                //break the message up based on breaking character
                String[] messageComponents = data.split(ComboServiceDetails.breakingCharacters);
                countRequests++;

                String response = null;

                //work out what command was sent and respond appropriately
                if(messageComponents[0].equalsIgnoreCase("echo"))
                {
                    //Strip out the echo and the breaking characters and bounce remainder back
                    response = data.replace("echo" + ComboServiceDetails.breakingCharacters, "");
                }
                else if(messageComponents[0].equalsIgnoreCase("daytime"))
                {
                    response = new Date().toString();
                }
                else if(messageComponents[0].equalsIgnoreCase("stats"))
                {
                    response = "Number of requests dealt with by the server is: " + countRequests;
                }
                else
                {
                    response = "Unrecognised request";
                }

                //get address of the sender
                InetAddress requestorAddress = incomingPacket.getAddress();

                byte[] responseBuffer = response.getBytes();

                //create packet to store the response
                DatagramPacket responsepacket = new DatagramPacket(responseBuffer, responseBuffer.length, requestorAddress, ComboServiceDetails.requestorListeningPort);

                //send the response
                providerSocket.send(responsepacket);
            }
        }
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(providerSocket != null)
            {
                providerSocket.close();
            }
        }
    }
}
