package com.personal.antanaszalisauskas.core;

public class ChatServiceDetails
{
    public static final int LISTENING_PORT = 50000;
    public static final String COMMAND_SEPARATOR = "%%";

    //command strings
    public static final String MESSAGE = "!MESSAGE";
    public static final String END_SESSION = "!QUIT";
    public static final String HELP_COMMANDS = "!HELP";

    //response strings
    public static final String SESSION_TERMINATED = "GOODBYE";
}
