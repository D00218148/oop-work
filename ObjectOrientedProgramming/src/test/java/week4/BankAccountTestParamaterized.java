/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import static org.junit.Assert.*;

/**
 *
 * @author D00218148
 */
@RunWith(Parameterized.class)
public class BankAccountTestParamaterized 
{
    private BankAccount account;
    private double amount;
    private boolean branch;
    private double expected;
    
    public BankAccountTestParamaterized(double amount, boolean branch, double expected)
    {
        this.amount = amount;
        this.branch = branch;
        this.expected = expected;
    }
    
    @org.junit.Before
    public void setup()
    {
        account = new BankAccount("Alex", "Lim", 300, 2);
        System.out.println("Setting up for test");
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> testConditions()
    {
        return Arrays.asList(new Object[][]
            {
                {100.0, true, 400.0},
                {200.0, true, 500.0},
                {325.14, true, 625.14},
                {489.33, true, 789.33},
                {1000.0, true, 1300.0}
            });
    }
    
    @org.junit.Test
    public void deposit()
    {
        account.deposit(amount, branch);
        assertEquals(expected, account.getBalance(), 0.0000001);
    }
}
