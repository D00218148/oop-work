package Server;

import Core.ComboServiceDetails;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class ComboTCPServer
{
    public static void main(String[] args)
    {
        try
        {
            //set up a connection socket
            ServerSocket listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);

            boolean continueRunning = true;

            while(continueRunning)
            {
                //wait for incoming connection
                Socket dataSocket = listeningSocket.accept();

                //build output and input objects
                OutputStream out = dataSocket.getOutputStream();
                PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

                InputStream in = dataSocket.getInputStream();
                Scanner input = new Scanner(new InputStreamReader(in));

                String incomingMessage = "";
                String response;

                while(!incomingMessage.equals(ComboServiceDetails.END_SESSION))
                {
                    response = null;

                    //take info from client
                    incomingMessage = input.nextLine();
                    System.out.println("received message: " + incomingMessage);

                    String[] messageComponents = incomingMessage.split(ComboServiceDetails.COMMAND_SEPARATOR);

                    if(messageComponents[0].equals(ComboServiceDetails.ECHO))
                    {
                        StringBuffer echoMessage = new StringBuffer("");

                        if(messageComponents.length > 1)
                        {
                            echoMessage.append(messageComponents[1]);
                            for(int i = 2; i < messageComponents.length; i++)
                            {
                                echoMessage.append(ComboServiceDetails.COMMAND_SEPARATOR);
                                echoMessage.append(messageComponents[i]);
                            }
                        }

                        response = echoMessage.toString();
                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.DAYTIME))
                    {
                        response = new Date().toString();
                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.STATS))
                    {

                    }
                    else if(messageComponents[0].equals(ComboServiceDetails.END_SESSION))
                    {
                        response = ComboServiceDetails.SESSION_TERMINATED;
                    }
                    else
                    {
                        response = ComboServiceDetails.URECOGNISED;
                    }

                    //send response back
                    output.println(response);
                    output.flush();
                }
                dataSocket.close();
            }
            listeningSocket.close();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
