/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week6;

/**
 *
 * @author D00218148
 */
public class Player 
{
    public String fullName;
    public int hitPoints;
    public String weapon;
    
    public void loseHealth(int damage)
    {
        this.hitPoints -= damage;
        if(this.hitPoints <= 0)
        {
            System.out.println("Player dead");
            //reduce lives
        }
    }
    
    public int healthRemaining()
    {
        return this.hitPoints;
    }
}
