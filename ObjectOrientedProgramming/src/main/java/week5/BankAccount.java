package week5;

public class BankAccount
{
    private String accountNo;
    private double balance;
    private String CustName;
    private String email;
    private String phoneNum;

    public BankAccount()
    {
        this("0000", 0.00, "Default name", "Default Address", "Default Phone");
    }

    public BankAccount(String CustName, String email, String phoneNum)
    {
        this("0000",50.00,CustName, email, phoneNum);
    }

    public BankAccount(String accountNo, double balance, String CustName, String email, String phoneNum)
    {
        System.out.println("Account constructor with parameters called");
        this.accountNo = accountNo;
        this.balance = balance;
        this.CustName = CustName;
        this.email = email;
        this.phoneNum = phoneNum;
    }

    public String getAccountNo()
    {
        return accountNo;
    }

    public double getBalance()
    {
        return balance;
    }

    public String getCustName()
    {
        return CustName;
    }

    public String getEmail()
    {
        return email;
    }

    public String getPhoneNum()
    {
        return phoneNum;
    }

    public void setAccountNo(String accountNo)
    {
        this.accountNo = accountNo;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public void setCustName(String custName)
    {
        CustName = custName;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setPhoneNum(String phoneNum)
    {
        this.phoneNum = phoneNum;
    }
    
    public void deposit(double depositAmount)
    {
        if(depositAmount > 0)
        {
            this.balance += depositAmount;
        }
        else
        {
            System.out.println("Deposit amount must be strictly positive");
        }
    }
    
    public void withdraw(double withdrawAmount)
    {
        if(withdrawAmount > 0 && (withdrawAmount <= this.balance))
        {
            this.balance -= withdrawAmount;
            System.out.println("Withdrawal of " + withdrawAmount + ". Current balance: " + this.balance);
        }
        else if(withdrawAmount <= 0)
        {
            System.out.println("Please select a strictly positive amount");
        }
        else
        {
            System.out.println("Only " + this.balance + "available. Withdraw not processed");
        }
    }
}

