/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.ArrayList;
/**
 *
 * @author D00218148
 */
public class GroceryList {
    private ArrayList<String> groceryList = new ArrayList<String>();
    
    public void addGroceryItem(String item)
    {
        groceryList.add(item);
    }
    
    public void printGroceryList()
    {
        System.out.println("You have " + groceryList.size() + " items");
        for(int i = 0; i < groceryList.size(); i++)
        {
            System.out.println((1+i) + ". " + groceryList.get(i));
        }
    }
    
    public void modifyGroceryItem(String currentItem, String newItem)
    {
        int position = findItem(currentItem);
        if(position >= 0)
        {
            modifyGroceryItem(position, newItem);
        }
    }
    
    private int findItem(String currentItem)
    {
        return groceryList.indexOf(currentItem);
    }
    
    private void modifyGroceryItem(int position, String newItem)
    {
        groceryList.set(position, newItem);
        System.out.println("Grocery item " + (position + 1) + " has been changed");
    }
    
    public void removeGroceryItem(String item)
    {
        int position = findItem(item);
        if(position >= 0)
        {
            removeGroceryItem(position);
        }
    }
    
    private void removeGroceryItem(int position)
    {
        groceryList.remove(position);
    }
    
    public boolean inList(String searchItem)
    {
        int position = findItem(searchItem);
        if(position >= 0)
        {
            return true;
        }
        else return false;
    }
}
