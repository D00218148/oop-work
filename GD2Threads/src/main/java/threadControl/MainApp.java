package threadControl;

public class MainApp
{
    public static void main(String[] args)
    {
        try
        {
            RepeatedMessage m1 = new RepeatedMessage("My favourite class is OOP", 500);
            RepeatedMessage m2 = new RepeatedMessage("My least favourite class is ASPM", 100);

            Thread m1t = new Thread(m1);
            Thread m2t = new Thread(m2);

            m1t.start();
            m2t.start();

            //pause to let the threads run then stop
            Thread.currentThread().sleep(5000);
            m1.finish();
            m2.finish();
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        //finally
        //{
        //    System.out.println();
        //}
    }
}
