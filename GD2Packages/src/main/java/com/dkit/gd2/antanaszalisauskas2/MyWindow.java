/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas2;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author D00218148
 */
public class MyWindow extends Frame
{
    public MyWindow(String title)
    {
        super(title);
        setSize(500, 140);
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                super.windowClosing(e);
                System.exit(0);
            }
        });
        
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        Font sansSerifLarge = new Font("SansSerif", Font.BOLD, 18);
        Font sansSerifSmall = new Font("SansSerif", Font.BOLD, 12);
        g.setFont(sansSerifLarge);
        g.drawString("I love JAVA", 60, 80);
        g.setFont(sansSerifSmall);
        g.drawString("I love JAVA", 60, 120);
    }
}
