package com.personal.antanaszalisauskas.client;

import com.personal.antanaszalisauskas.core.ChatServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ChatServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public ChatServiceThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run()
    {
        //loop
        //wait for message
        String incomingMessage = "";
        String response;
        //process message
        try
        {
            while(!incomingMessage.equals(ChatServiceDetails.END_SESSION))
            {
                response = null;
                incomingMessage = input.nextLine();
                System.out.println("received message: " + incomingMessage);

                String[] messageComponents = incomingMessage.split(ChatServiceDetails.COMMAND_SEPARATOR);
                if(messageComponents[0].equals(ChatServiceDetails.MESSAGE))
                {
                    StringBuffer sendMessage = new StringBuffer("");

                    if(messageComponents.length > 1)
                    {
                        sendMessage.append(messageComponents[1]);
                        for(int i = 2; i < messageComponents.length; i++)
                        {
                            sendMessage.append(ChatServiceDetails.COMMAND_SEPARATOR);
                            sendMessage.append(messageComponents[i]);
                        }
                    }

                    response = sendMessage.toString();
                }
                else if(messageComponents[0].equals(ChatServiceDetails.HELP_COMMANDS))
                {
                    response = "Enter !QUIT to leave chat room. Any message without '!' will be treated as a normal message";
                }
                else
                {
                    response = "Command not recognised. Type !HELP for available commands.";
                }

                //send response
                output.println(response);
                output.flush();
            }
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("Closing connection with client #" + number);
                dataSocket.close();
            }
            catch(IOException e)
            {
                System.out.println("Unable to disconnect + " + e.getMessage());
                System.exit(1);
            }
        }
    }
}
