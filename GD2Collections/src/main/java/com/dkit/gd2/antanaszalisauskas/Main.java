package com.dkit.gd2.antanaszalisauskas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main
{
    public static void main(String[] args) {
        
        ImmutableLocation testLocation = new ImmutableLocation(0, "P1107", new HashMap<String, Integer>());
        
        /*
        Theatre omniplexDundalk = new Theatre("Omniplex Dundalk", 6, 7);
        printList(omniplexDundalk.seats);

        Collections.sort(omniplexDundalk.seats, new SeatComparatorByPrice());
        printList(omniplexDundalk.seats);

        
        List<Theatre.Seat> seatCopy = new ArrayList<Theatre.Seat>(omniplexDundalk.seats);
        
        printList(seatCopy);
        seatCopy.get(1).reserve();
        
        Collections.shuffle(seatCopy);
        printList(seatCopy);
        
        bubbleSort(seatCopy);
        printList(seatCopy);
        
        //seatCopy is a shallow copy of the seats from omniplex
        //seatCopy and seats are 2 separate arraylists
        
        if(omniplexDundalk.reserveSeat("A02"))
        {
            System.out.println("Please pay");
        }
        else
        {
            System.out.println("Reserved");
        }
        
        Theatre.Seat minSeat = Collections.min(seatCopy);
        System.out.println("Min seat is " + minSeat.getSeatNumber()); //A01
        Theatre.Seat maxSeat = Collections.max(seatCopy);
        System.out.println("Max seat is " + maxSeat.getSeatNumber()); //H12
        
        
        //homework - magic square, explain why deep copy throws exception
        List<Theatre.Seat> notDeepCopy = new ArrayList(omniplexDundalk.seats.size());
        for(int i = 0; i < 96; i++)
        {
            notDeepCopy.add(omniplexDundalk.new Seat("A01", 0.00));
        }
        Collections.copy(notDeepCopy, omniplexDundalk.seats);
        System.out.println(omniplexDundalk.seats);
        System.out.println(seatCopy);
        System.out.println(notDeepCopy);
         */

    }
    
    //order n^2 complexity - it will be slow with larger lists
    public static void bubbleSort(List<? extends Theatre.Seat> list)
    {
        for(int i = 0; i < list.size(); i++)
        {
            for(int j = i+1; j < list.size(); j++)
            {
                if(list.get(i).compareTo(list.get(j)) > 0)
                {
                    Collections.swap(list, i, j);
                }
            }
        }
    }
    
    public static void printList(List<Theatre.Seat> list)
    {
        for(Theatre.Seat seat: list)
        {
            System.out.println(" " + seat.getSeatNumber() + " €" + String.format("%.2f",seat.getPrice()));
        }
        System.out.println("------------------------------------------");
    }
}
