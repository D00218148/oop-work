package week5;
/*
A vip has a name, creditLimit and emailAddress
need 3 constructors - one that takes three parameters
one that takes 2 parameters and sets default email address
last one is a default contructor
use contructor chaining
 */

public class VIP
{
    private String name;
    private double creditLimit;
    private String emailAddress;

    public VIP()
    {
        this("Default name", 00.00, "Default email");
    }

    public VIP(String name, double creditLimit)
    {
        this(name, creditLimit, "Default email");
    }

    public VIP(String name, double creditLimit, String emailAddress)
    {
        this.name = name;
        this.creditLimit = creditLimit;
        this.emailAddress = emailAddress;
    }
}
