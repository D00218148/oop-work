package Core;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TaskManagerServiceDetails
{
    public static final String BREAKING_CHARACTERS = "%%";

    public static final String HOSTNAME = "localhost";
    public static final int LISTENING_PORT = 23400;

    public static final String ADD_COMMAND = "add";
    public static final String SUCCESSFUL_ADD = "ADDED";
    public static final String FAILED_ADD = "FAILED";

    public static final String REMOVE_COMMAND = "remove";
    public static final String SUCCESSFUL_REMOVE = "DELETED";
    public static final String FAILED_REMOVE = "NOT_FOUND";

    public static final String VIEW_COMMAND = "viewAll";
    public static final String TASK_SEPARATOR = "##";

    public static final String EXIT_COMMAND = "EXIT";
    public static final String SIGN_OFF = "GOODBYE";
    public final static int ARGUMENTS_FOR_ADD = 4;
    public final static int ARGUMENTS_FOR_REMOVE = 2;
    public final static int ARGUMENTS_FOR_VIEW = 1;
    public final static int TASK_NAME_INDEX = 1;
    public final static int TASK_OWNER_INDEX = 2;
    public final static int TASK_DATE_INDEX = 3;


    public static String flattenTaskSet(Set<Task> tasks)
    {
        if(!tasks.isEmpty())
        {
            StringBuilder taskString = new StringBuilder();
            for(Task t: tasks)
            {
                taskString.append(t.format());
                taskString.append(TaskManagerServiceDetails.TASK_SEPARATOR);
            }
            return taskString.toString().substring(0, taskString.toString().length()-2);
        }
        return null;
    }

    public static Set<Task> recreateTaskSet(String taskString)
    {
        Set<Task> tasks = new HashSet<>();
        String[] taskStrings = taskString.split(TaskManagerServiceDetails.TASK_SEPARATOR);
        for(String task: taskStrings)
        {
            String[] components = task.split(TaskManagerServiceDetails.BREAKING_CHARACTERS);
            if(components.length == 3)
            {
                try
                {
                    long deadlineTime = Long.parseLong(components[2]);
                    Date deadline = new Date(deadlineTime);
                    Task t = new Task(components[0], components[1], deadline);
                    tasks.add(t);
                }
                catch(NumberFormatException e)
                {
                    System.out.println(e.getMessage());
                }
            }
        }
        return tasks;
    }
}
