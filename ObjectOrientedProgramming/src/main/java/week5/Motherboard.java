package week5;

public class Motherboard
{
    private String model;
    private String manufacturer;
    private int ramSlots;
    private int cardSlots;
    private String chipSet;
    private int sataPorts;
    private int ramCapacity;
    private String bios;

    public Motherboard(String model, String manufacturer, int ramSlots, int cardSlots, String chipSet, int sataPorts, int ramCapacity, String bios) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.ramSlots = ramSlots;
        this.cardSlots = cardSlots;
        this.chipSet = chipSet;
        this.sataPorts = sataPorts;
        this.ramCapacity = ramCapacity;
        this.bios = bios;
    }

    public void loadProgram(String programName)
    {
        System.out.println("Program " + programName + " is loading......");
    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getRamSlots() {
        return ramSlots;
    }

    public int getCardSlots() {
        return cardSlots;
    }

    public String getChipSet() {
        return chipSet;
    }

    public int getSataPorts() {
        return sataPorts;
    }

    public int getRamCapacity() {
        return ramCapacity;
    }

    public String getBios() {
        return bios;
    }
}
