package ColouredThreads;

import static ColouredThreads.ThreadColour.*;

public class MainApp
{
    public static void main(String[] args)
    {
        System.out.println(ANSI_PURPLE + "Hell from main thread");

        Thread anotherThread = new AnotherThread();
        anotherThread.setName("== Another Thread ==");
        anotherThread.start();

        new Thread()
        {
            public void run()
            {
                System.out.println(ANSI_GREEN + "Hell from the anonymous thread");
            }
        }.start();

        Thread myRunnableThread = new Thread(new MyRunnable()
        {
            @Override
            public void run()
            {
                System.out.println(ANSI_CYAN + "Hell from the anonymous runnable implementation of run");
                try
                {
                    anotherThread.join();
                    System.out.println(ANSI_CYAN + "ANother thread terminated, or timed out, so I am running again");
                }
                catch(InterruptedException e)
                {
                    System.out.println(ANSI_CYAN + "I could not wait, i was interrupted");
                }
            }
        });

        myRunnableThread.start();
        System.out.println(ANSI_PURPLE + "hell again from main thread");
    }
}
