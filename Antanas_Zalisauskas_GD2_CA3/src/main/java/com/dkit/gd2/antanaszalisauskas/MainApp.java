package com.dkit.gd2.antanaszalisauskas;



/**
 *
 * @author 
 */
public class MainApp
{

    public static void main(String[] args)
    {
        Staff s1 = new Permanent("ID123", "Mary Ryan", 230, "Dublin", 55.70, 100);
        Staff s2 = new Permanent("ID125", "Tom Brady", 130, "Belfast", 45.70, 150);
        Staff s3 = new Contract("ID128", "John Browne", 160, "Belfast", 72.50, 120, 160);
        Staff s4 = new Contract("ID120", "Jane Reilly", 156, "London", 142.50, 250, 120);
        Staff s5 = new Permanent("ID123", "Brendan Brady", 130, "Dublin", 55.70, 200);

        Flight f1 = new Flight("FL123", "Dublin", "London");

        
        System.out.println("\nQ3 - AddStaff");
        f1.addStaff(s1);
        f1.addStaff(s2);
        f1.addStaff(s3);
        f1.addStaff(s4);
        f1.addStaff(s5); // test for duplicate

        Staff s6 = null;
        f1.addStaff(s6);  //test for null
////        
        System.out.println("\nQ4 - displayStaff()");
        f1.displayStaff();
////        
        System.out.println("\nQ5 - displayNamesWithFlightHours()");
        f1.displayNamesWithFlightHours();
//
        System.out.println("\nQ6");
        f1.sortStaff();
        f1.displayStaff();
////        
        System.out.println("\nQ7");
        f1.sortStaffByNameWithinAirport();
        f1.displayStaff();
//
        System.out.println("\nQ8");
        f1.updateAllStaffBonus();
        f1.displayStaff();
//
        
//        Note that for initial testing of readStaffFromFile(), this code creates a new Flight f2. 
        System.out.println("\nQ10 - read staff data from file");
        Flight f2 = new Flight("FL124", "Dublin", "Paris");
        f2.readStaffFromFile("staff.txt");
        f2.displayStaff();
//        
    }

}
