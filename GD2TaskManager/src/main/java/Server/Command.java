package Server;

import Core.TaskManager;

public interface Command
{
    public String generateResponse(String[] components, TaskManager taskList);
}
