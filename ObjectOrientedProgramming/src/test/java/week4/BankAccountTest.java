/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author D00218148
 */
public class BankAccountTest {
    private BankAccount account;
    private static int count;
    
    public BankAccountTest() {
    }
    
    @BeforeClass
    public static void setUpClass() 
    {
        System.out.println("This executes before any test cases. Count = " + count++);
    }
    
    @AfterClass
    public static void tearDownClass() 
    {
        System.out.println("This happens after all test cases");
    }
    
    @org.junit.Before
    public void setUp() 
    {
        BankAccount account = new BankAccount("Ant", "Boyo", 500.00, 1);
        System.out.println("Running Setup...");
    }
    
    @After
    public void tearDown() 
    {
        System.out.println("Count = " + count++);
    }

    /**
     * Test of deposit method, of class BankAccount.
     */
    @org.junit.Test
    public void testDeposit() {
        System.out.println("deposit");
        double amount = 275.00;
        boolean branch = true;
        double expResult = 775.00;
        double result = account.deposit(amount, branch);
        assertEquals(expResult, result, 0);
    }

    @org.junit.Test
    public void getBalanceDeposit()
    {
        double amount = 275.00;
        boolean branch = true;
        double expResult = 775.00;
        double result = account.deposit(amount, branch);
        assertEquals(expResult, account.getBalance(), 0);
    }
    
    @org.junit.Test
    public void isCheckingTrue()
    {
        assertTrue("The account is not a checking account", account.isChecking());
    }
    
    @org.junit.Test(expected = IllegalArgumentException.class)
    public void withdrawATM()
    {
        double balance = account.withdraw(600, true);
        double expResult = 400;
        assertEquals(expResult, balance, 0);
    }
    /**
     * Test of withdraw method, of class BankAccount.
     */
    @org.junit.Test
    public void testWithdraw() {
        System.out.println("withdraw");
        double amount = 300.00;
        boolean branch = false;
        double expResult = 200.00;
        double result = account.withdraw(amount, branch);
        assertEquals(expResult, result, 0.0);
    }

    @org.junit.Test
    public void getBalanceWithdraw()
    {
        double amount = 200.00;
        boolean branch = true;
        double expResult = 300.00;
        double result = account.withdraw(amount, branch);
        assertEquals(expResult, account.getBalance(), 0);
    }
    
    /**
     * Test of getbalance method, of class BankAccount.
     */
    @org.junit.Test
    public void testGetbalance() {
        System.out.println("getbalance");
        BankAccount instance = null;
        double expResult = 0.0;
        double result = instance.getBalance();
        assertEquals(expResult, result, 0.0);
    }
    
}
