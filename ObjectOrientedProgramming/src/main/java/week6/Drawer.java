package week6;

public class Drawer
{
    private int length;
    private int width;
    private int height;
    private int drawers;
    private String material;
    private boolean hasLegs;

    public Drawer(int length, int width, int height, int drawers, String material, boolean hasLegs) {
        this.length = length;
        this.width = width;
        this.height = height;
        this.drawers = drawers;
        this.material = material;
        this.hasLegs = hasLegs;
    }

    public void openDrawer()
    {
        System.out.println("opening drawer");
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDrawers() {
        return drawers;
    }

    public String getMaterial() {
        return material;
    }

    public boolean isHasLegs() {
        return hasLegs;
    }
}
