package Tutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CounterMain
{
    public static void main(String[] args)
    {
        Counter counter = new Counter();
        Runnable runner = new CounterThread(counter);
        Scanner sc = new Scanner(System.in);
        List<Thread> threads = new ArrayList();

        System.out.println("Enter number of threads");
        int threadsNum = sc.nextInt();
        sc.nextLine();
        System.out.println("Enter number of loops");
        int loops = sc.nextInt();
        sc.nextLine();

        for(int i = 0; i < threadsNum; i++)
        {
            threads.add(new Thread(runner));
        }


        System.out.println("expected: " + (threadsNum*loops));
        System.out.println("Actual: " + counter.getCount());

    }
}
