package com.personal.antanaszalisauskas.server;

import com.personal.antanaszalisauskas.client.ChatServiceThread;
import com.personal.antanaszalisauskas.core.ChatServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ChatServiceServer
{
    public static void main(String[] args)
    {
        try
        {
            //setup listening socket
            ServerSocket listeningSocket = new ServerSocket(ChatServiceDetails.LISTENING_PORT);

            //setup thread group to manage all client threads
            ThreadGroup clientThreadGroup = new ThreadGroup("Client threads");

            //place more emphasis on accepting clients than processing them
            //by setting their priority to be one less than the main thread
            clientThreadGroup.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //do the main logic of server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning)
            {
                //wait for incoming connection and build communication link
                Socket dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("There are " + threadCount + " clients currently on this server");

                //build the thread
                //need to give thread a few things:
                //1. group it needs to be stored in
                //2. name for thread
                //3. socket to communicate through
                //4. any other info
                ChatServiceThread newClient = new ChatServiceThread(clientThreadGroup, dataSocket.getInetAddress()+"", dataSocket, threadCount);
                newClient.start();
            }
            listeningSocket.close();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
