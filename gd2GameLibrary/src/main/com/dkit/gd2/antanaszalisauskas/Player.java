package src.main.com.dkit.gd2.antanaszalisauskas;

import java.util.ArrayList;
import java.util.List;

public class Player implements ISavable
{
    private String name;
    private int hitPoints;
    private int strength;
    private String weapon;

    public Player(String name, int hitPoints, int strength) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
        this.weapon = "sword";
    }

    public String getName() {
        return name;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public int getStrength() {
        return strength;
    }

    public String getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", hitPoints=" + hitPoints +
                ", strength=" + strength +
                ", weapon='" + weapon + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;

        Player player = (Player) o;

        if (getHitPoints() != player.getHitPoints()) return false;
        if (getStrength() != player.getStrength()) return false;
        if (getName() != null ? !getName().equals(player.getName()) : player.getName() != null) return false;
        return getWeapon() != null ? getWeapon().equals(player.getWeapon()) : player.getWeapon() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + getHitPoints();
        result = 31 * result + getStrength();
        result = 31 * result + (getWeapon() != null ? getWeapon().hashCode() : 0);
        return result;
    }
    
    public List<String> write()
    {
        List<String> values = new ArrayList<String>();
        values.add(0, this.name);
        values.add(1, "" + this.getHitPoints());
        values.add(2, "" + this.getStrength());
        values.add(3, this.weapon);
        return values;
    }

    public void read(List<String> savedValues) {
        if(savedValues != null && savedValues.size() > 0)
        {
            this.name = savedValues.get(0);
            this.hitPoints = Integer.parseInt(savedValues.get(1));
            this.strength = Integer.parseInt(savedValues.get(2));
            this.weapon = savedValues.get(3);
        }
    }
}
