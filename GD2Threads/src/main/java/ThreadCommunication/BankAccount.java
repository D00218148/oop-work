package ThreadCommunication;

public class BankAccount
{
    private double balance = 0;
    private boolean isOpen = true;

    /*
    This method withdraws an amount from the bank account.
    If the funds are insufficient, it'll wait until the
    funds are available or the account is closed
    @param amount - the amount to withdraw from the account
    @return true if the withdraw is successful, false otherwise
    @exception InterruptedException if another thread interrupts
     */

    public synchronized boolean withdraw(int amount)
    {
        while(amount > balance && isOpen())
        {
            System.out.println("waiting for sufficient money...");
            try
            {
                wait();
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }

        boolean result = false;
        if(isOpen())
        {
            balance -= amount;
            result = true;
        }
        return result;
    }

    /**
     * checks if the account is open
     * @return true if open, false if not
     */
    public synchronized boolean isOpen()
    {
        return isOpen;
    }

    /**
     * Deposits an amount assuming that the account is open
     * when the deposit i successful it will notify one
     * waiting thread at random
     * @param amount
     * @return true is deposit is successful, otherwise false
     */
    public synchronized boolean deposit(int amount)
    {
        if(isOpen())
        {
            balance += amount;
            notify();
            return true;
        }
        return false;
    }

    public synchronized void close()
    {
        isOpen = false;
        notifyAll();
    }
}
