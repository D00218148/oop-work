/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Solutions;
import java.util.Arrays;
import java.util.Scanner;
/**
 *
 * @author Antanas Zalisauskas - D00218148
 */
public class Q1_To_Q4 
{
    private static Scanner keyboard = new Scanner(System.in);
    
    public static void main(String[] args) 
    {
        boolean quit = false;
        
        while(quit == false)
        {
            System.out.println("Enter question choice:");
            System.out.println("1 - To run Q2");
            System.out.println("2 - To run Q3");
            System.out.println("3 - To exit");
            int input = keyboard.nextInt();

            switch(input)
            {
                case 1:
                    question2();
                    break;
                case 2:
                    question3();
                    break;
                case 3:
                    quit = true;
                    break;
            }
        }
    }
    
    private static void question2()
    {
        int[][] matrix1 = 
        {
            {5, 3, 9},
            {12, 2, 2},
            {30, 35, 29},
            {10, 74, 33}
        };
        
        int[][] matrix2 = 
        {
            {11, 4, 12},
            {18, 21, 1},
            {0, 44, 29},
            {90, 6, 34}
        };
        
        int[][] resultMatrix = addMatrices(matrix1, matrix2);
        
        System.out.println("Result of matrix 1 + matrix 2");
        printMatrix(resultMatrix);
    }
    
    private static int[][] addMatrices(int[][] matrix1, int[][] matrix2)
    {
        int[][] resultMatrix = new int[4][3];

        for(int i = 0; i < matrix1.length; i++)
        {
            for(int j = 0; j < matrix1[i].length; j++)
            {
                resultMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        
        return resultMatrix;
    }
    
    private static void printMatrix(int[][] matrixToPrint)
    {
        for(int i = 0; i < matrixToPrint.length; i++)
        {
            for(int j = 0; j < matrixToPrint[i].length; j++)
            {
                System.out.print(matrixToPrint[i][j] + "\t");
            }
            System.out.println();
        }
    }
    
    private static void question3()
    {
        int[] numberArray = {12, 9, 11, 33, 7, 10};
        int[] indexArray = indexOfMinAndMax(numberArray);
        
        System.out.println("Number array: " + Arrays.toString(numberArray));
        System.out.println("Min of Array: " + numberArray[indexArray[0]] + ", Max of Array: " + numberArray[indexArray[1]]);
    }
    
    private static int[] indexOfMinAndMax(int[] numberArray)
    {
        int min = 0;
        int max = 0;
        
        for(int i = 0; i < numberArray.length - 1; i++)
        {
            if(numberArray[i] < numberArray[i + 1])
            {
                min = i;
            }
            if(numberArray[i] > numberArray[i + 1])
            {
                max = i;
            }
        }
        
        int[] indexArray = {min, max};
        return indexArray;
    }
}
