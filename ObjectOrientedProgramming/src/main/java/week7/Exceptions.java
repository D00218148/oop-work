package week7;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Exceptions
{
    public static void main(String[] args)
    {
        try
        {
            int result = divide();
            System.out.println(result);
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
    }

    private static int divide()
    {
        Scanner sc = new Scanner(System.in);
        int x = 1;
        int y = 1;

        try
        {
            x = getInt();
            y = getInt();
            System.out.println("x is " + x + ", y is " + y);
            return x/y;
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Can you don't enter control D. This closes System.in meaning nothing can be done with application");
        }
        /*
        catch(ArithmeticException e)
        {
            System.out.println("Y must be non-zero. Cannot divide by zero.");

        }
        */
        catch(ArithmeticException e)
        {
            while(true)
            {
                y = getInt();
                if(y != 0)
                {
                    return x/y;
                }
                else
                {
                    System.out.println("Y must be non-zero. Cannot divide by zero.");
                }
            }

        }

    }

    public static int getInt()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter an integer ");
        while(true)
        {
            try
            {
                return sc.nextInt();
            }
            catch(InputMismatchException e)
            {
                sc.nextLine();
                System.out.println("Please enter a number using only digits 0 to 9");
            }
        }

    }
}
