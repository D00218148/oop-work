
package com.dkit.gd2.antanaszalisauskas;



/**
 *
 * @author 
 */
public class Contract extends Staff
{
    private int topUpBonus;
    private int numberOfFlightHours;

    public Contract(String id, String name, int bonus, String baseAirport, double baseAirport1, int topUpBonus, int numberOfFlightHours) {
        super(id, name, bonus, baseAirport, baseAirport1);
        this.topUpBonus = topUpBonus;
        this.numberOfFlightHours = numberOfFlightHours;
    }

    public int getTopUpBonus() {
        return topUpBonus;
    }

    public int getNumberOfFlightHours() {
        return numberOfFlightHours;
    }

    @Override
    public String getId() {
        return super.getId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getBonus() {
        return super.getBonus();
    }

    @Override
    public String getBaseAirport() {
        return super.getBaseAirport();
    }

    @Override
    public double getBaseAirport1() {
        return super.getBaseAirport1();
    }

    @Override
    public String toString() {
        return super.toString() + ", top up bonus=" + topUpBonus + ", number of flight hours=" + numberOfFlightHours;
    }

    public void updateBonus(Flight flight)
    {
        if(!this.baseAirport.equals(flight.getDestination()) && this.numberOfFlightHours > 100)
        {
            this.bonus = this.bonus + this.topUpBonus;
        }
    }
}
