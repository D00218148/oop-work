package src.main.com.dkit.gd2.antanaszalisauskas;

import java.util.List;

public interface ISavable
{
    List<String> write();
    void read(List<String> savedValues);
}
