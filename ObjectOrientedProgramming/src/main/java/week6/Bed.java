package week6;

public class Bed
{
    private int width;
    private int length;
    private int height;
    private int pillows;
    private String material;

    public Bed(int width, int length, int height, int pillows, String material) {
        this.width = width;
        this.length = length;
        this.height = height;
        this.pillows = pillows;
        this.material = material;
    }

    public void sleep()
    {
        System.out.println("Engaging sleep mode");
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    public int getPillows() {
        return pillows;
    }

    public String getMaterial() {
        return material;
    }
}
