package ThreadPipes;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;

public class PipeDemo extends Thread
{
    PipedOutputStream output;

    public PipeDemo(PipedOutputStream out)
    {
        output = out;
    }

    public void run()
    {
        try
        {
            PrintStream p = new PrintStream(output);

            //print message
            p.println("Hello from another thread, using pipes");

            //close the stream
            p.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        try
        {
            //create pipe for writing
            PipedOutputStream pout = new PipedOutputStream();

            //create pipe for reading and connected to output pipe
            PipedInputStream pin = new PipedInputStream(pout);

            PipeDemo p1 = new PipeDemo(pout);

            p1.start();

            //read thread data
            int input = pin.read();

            //terminate when the end of the stream is reached
            while(input != -1)
            {
                //print message
                System.out.print((char)input);
                //read next byte
                input = pin.read();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
