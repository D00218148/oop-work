package Core;

public class LottoServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    //breaking characters
    public static final String COMMAND_SEPARATOR = "%";

    //command Strings
    public static final String GENERATE = "GENERATE";
    public static final String CLOSE = "***CLOSE***";

    //response strings
    public static final String GENERATED = "GENERATED";
    public static final String SESSION_TERMINATED = "GOODBYE";
    public static final String UNRECOGNISED = "UNKNOWN COMMAND";
}
