package Server;

import Core.Task;
import Core.TaskManager;
import Core.TaskManagerServiceDetails;

public class RemoveCommand implements Command
{
    @Override
    public String generateResponse(String[] components, TaskManager taskList) {
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_REMOVE)
        {
            String taskName = components[TaskManagerServiceDetails.TASK_NAME_INDEX];
            Task taskToRemove = new Task(taskName);
            boolean removed = taskList.remove(taskToRemove);
            if(removed)
            {
                response = TaskManagerServiceDetails.SUCCESSFUL_REMOVE;
            }
            else
            {
                response = TaskManagerServiceDetails.FAILED_REMOVE;
            }
        }
        return response;
    }
}
