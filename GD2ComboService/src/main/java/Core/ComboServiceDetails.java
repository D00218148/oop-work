package Core;

public class ComboServiceDetails
{
    static final public int requestorListeningPort = 50001;
    static final public int providerListeningPort = 50000;
    static final public String breakingCharacters = "&&";
    static final public int MAX_LEN = 150;
}
