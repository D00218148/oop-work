package Client;

import Core.ComboServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboStreamClient
{
    public static void main(String[] args) {
        try
        {
            //Step 1: establish a connection with the server
            //Like a phone call, first thing you do is
            //dial the number you want to talk to
            Socket dataSocket = new Socket("86.40.53.44", ComboServiceDetails.LISTENING_PORT);

            //Step 2 build output and input streams
            OutputStream out = dataSocket.getOutputStream();
            //wrapped in printWriter for efficiency
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";

            while(!message.equals(ComboServiceDetails.END_SESSION))
            {
                displayMenu();
                int choice = getNumber(keyboard);
                String response = "";

                if(choice >= 0 && choice < 3)
                {
                    switch(choice)
                    {
                        case 0:
                            message = ComboServiceDetails.END_SESSION;

                            //send message
                            output.println(message);
                            output.flush();

                            response = input.nextLine();
                            if(response.equals(ComboServiceDetails.SESSION_TERMINATED))
                            {
                                System.out.println("Session ended");
                            }
                            break;
                        case 1:
                            message = generateEcho(keyboard);

                            //send message
                            output.println(message);
                            output.flush();

                            //get response
                            response = input.nextLine();

                            System.out.println("received response: " + response);
                            break;
                        case 2:
                            message = ComboServiceDetails.DAYTIME;

                            //send message
                            output.println(message);
                            output.flush();

                            response = input.nextLine();
                            System.out.println("The current date and time is: " + response);
                            break;
                    }
                    if(response.equals(ComboServiceDetails.URECOGNISED))
                    {
                        System.out.println("not recognised");
                    }
                }
                else
                {
                    System.out.println("please select an option from the menu");
                }
            }
            System.out.println("Thank you for using our thing");
            dataSocket.close();
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void displayMenu()
    {
        System.out.println("0 to exit");
        System.out.println("1 to echo a message");
        System.out.println("2 to get current time");
    }

    public static int getNumber(Scanner keyboard)
    {
        boolean numberEntered = false;
        int number = 0;
        while(!numberEntered)
        {
            try
            {
                number = keyboard.nextInt();
                numberEntered = true;
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number: ");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return number;
    }

    public static String generateEcho(Scanner keyboard)
    {
        //setup the command text
        StringBuffer message = new StringBuffer(ComboServiceDetails.ECHO);
        message.append(ComboServiceDetails.COMMAND_SEPARATOR);
        //get the message to be echoed
        System.out.println("What would you like to echo:");
        String echo = keyboard.nextLine();
        message.append(echo);

        return message.toString();
    }
}
