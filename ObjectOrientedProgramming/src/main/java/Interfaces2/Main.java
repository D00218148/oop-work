package Interfaces2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Player bob = new Player("Bob", 200, 1);
        System.out.println(bob);
        saveObject(bob);



        bob.setHitPoints(8);
        System.out.println(bob);
        saveObject(bob);
        //loadObject(bob);
        System.out.println(bob);

        ISavable werewolf = new Monster("Werewolf", 20, 40);
        System.out.println("Strength = " + ((Monster)werewolf).getStrength());
        saveObject(werewolf);
    }

    public static void saveObject(ISavable objectToSave)
    {
        for(int i = 0; i < objectToSave.write().size(); i++)
        {
            System.out.println("Saving " + objectToSave.write().get(i) + " to storage device");
        }
    }

    public static void loadObject(ISavable objectToLoad)
    {
        List<String> values = readValues();
        objectToLoad.read(values);
    }

    public static List<String> readValues()
    {
        List<String> values = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);
        boolean quit = false;
        int index = 0;
        System.out.println("Choose \n" + "1 to enter String\n" + "0 to quit");
        while(!quit)
        {
            System.out.println("Choose option:");
            int choice = sc.nextInt();
            sc.nextLine();
            switch(choice)
            {
                case 0:
                    quit = true;
                    break;
                case 1:
                    System.out.println("Enter String: ");
                    String stringInput = sc.nextLine();
                    values.add(index, stringInput);
                    index++;
            }
        }
        return values;
    }

}
