package Generics;

import java.util.ArrayList;

public class Team<T extends Player> implements Comparable<Team>
{
    private String name;
    private int games;
    private int wins;
    private int losses;
    private int tied;

    private ArrayList<T> members = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addPlayer(T player)
    {
        if(members.contains(player))
        {
            System.out.println(player.getName() + " is already in the team");
            return false;
        }
        else
        {
            members.add(player);
            System.out.println(player.getName() + " is picked for " + this.name);
            return true;
        }
    }

    public int numPlayers()
    {
        return this.members.size();
    }

    public void matchResult(Team<T> opponent, int ourScore, int theirScore)
    {
        if(ourScore > theirScore)
        {
            wins++;
        }
        else if(ourScore == theirScore)
        {
            tied++;
        }
        else
        {
            losses++;
        }
        games++;
        if(opponent != null)
        {
            opponent.matchResult(null, theirScore, ourScore);
        }
    }

    public int ranking()
    {
        return (wins*2) + tied;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    @Override
    public int compareTo(Team team) {
        if(this.ranking() > team.ranking())
        {
            return -1;
        }
        else if(this.ranking() < team.ranking())
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}
