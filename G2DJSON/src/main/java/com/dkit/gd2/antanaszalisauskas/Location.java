package com.dkit.gd2.antanaszalisauskas;

import java.util.Map;

public class Location
{
    private String message;
    private String timestamp;
    private Map<String, Object> iss_position;

    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Map<String, Object> getIss_position() {
        return iss_position;
    }

    @Override
    public String toString() {
        return "Location{" +
                "message='" + message + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", location='" + iss_position + '\'' +
                '}';
    }
}
