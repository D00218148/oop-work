/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import DTOs.User;
import Exceptions.DaoException;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author D00218148
 */
public class MySQLUserDAO extends MySqlDao implements IUserDaoInterface
{

    public List<User> findAllUsers() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<User> users = new ArrayList();
        
        try
        {
            con = this.getConnection();
            String query = "select * from user";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next())
            {
                int userId = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String lastName = rs.getString("last_name");
                String firstName = rs.getString("first_name");
                
                User u = new User(userId, lastName, firstName, username, password);
                users.add(u);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findAllUsers() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
        
        return users;
    }

    public User findUserByUsernamePassword(String username, String password) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;
        
        try
        {
            con = this.getConnection();
            String query = "select * from user where username = ? and password = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            
            rs = ps.executeQuery();
            
            if(rs.next())
            {
                int userId = rs.getInt("id");
                String uName = rs.getString("username");
                String pWord = rs.getString("password");
                String lastName = rs.getString("last_name");
                String firstName = rs.getString("first_name");
                
                User us = new User(userId, lastName, firstName, uName, pWord);
                
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("findUserByUsernamePassword()" + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("findAllUsers() " + e.getMessage());
            }
        }
        return u;
    }
    
}
