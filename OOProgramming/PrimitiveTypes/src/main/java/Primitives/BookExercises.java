/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.Arrays;
import java.util.Random;
/**
 *
 * @author Antanas Zalisauskas
 */
public class BookExercises 
{
    public static void main(String[] args)
    {
        //6.14
        Random rand = new Random();
        int[] numbers = new int[20];
        
        for(int i = 0; i < numbers.length; i++)
        {
            numbers[i] = rand.nextInt(100);
        }
        System.out.println("Unsorted array = " + Arrays.toString(numbers));
        sortArray(numbers);
        System.out.println("Sorted array = " + Arrays.toString(numbers));
    }
    
    //6.14
    private static void sortArray(int[] numbers)
    {
        Arrays.sort(numbers);
    }
}
