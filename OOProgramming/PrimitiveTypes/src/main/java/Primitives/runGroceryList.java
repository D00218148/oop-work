/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.Scanner;
import java.util.ArrayList;
/**
 *
 * @author D00218148
 */
public class runGroceryList 
{
    private static Scanner keyboard = new Scanner(System.in);
    private static GroceryList groceryList = new GroceryList();
    
    public static void main(String[] args) 
    {
        boolean quit = false;
        int choice = 0;
        printInstructions();
        
        while(quit == false)
        {
            System.out.println("Enter your choice");
            choice = keyboard.nextInt();
            keyboard.nextLine();
            
            switch(choice)
            {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    groceryList.printGroceryList();
                    break;
                case 2:
                    addItem();
                    break;
                case 3:
                    modifyItem();
                    break;
                case 4:
                    removeItem();
                    break;
                case 5:
                    searchItem();
                    break;
                case 6:
                    quit = true;
                    break;
            }
        }
    }
    
    public static void printInstructions()
    {
        System.out.println("\nPress");
        System.out.println("\t 0 - To print choice options");
        System.out.println("\t 1 - To print grocery list");
        System.out.println("\t 2 - To add item to grocery list");
        System.out.println("\t 3 - To modify list");
        System.out.println("\t 4 - To remove an item");
        System.out.println("\t 5 - To search for an item");
        System.out.println("\t 6 - To quit application");
    }
    
    public static void addItem()
    {
        System.out.println("Please enter grocery item");
        groceryList.addGroceryItem(keyboard.nextLine().toLowerCase());
    }
    
    public static void modifyItem()
    {
        System.out.println("Enter item name: ");
        String oldItem = keyboard.nextLine();
        System.out.println("Enter replacement item: ");
        String newItem = keyboard.nextLine();
        groceryList.modifyGroceryItem(oldItem.toLowerCase(), newItem.toLowerCase());
    }
    
    public static void removeItem()
    {
        System.out.println("Enter item name: ");
        String itemName = keyboard.nextLine();
        groceryList.removeGroceryItem(itemName.toLowerCase());
    }
    
    public static void searchItem()
    {
        System.out.println("Please enter item to search: ");
        String searchItem = keyboard.nextLine();
        if(groceryList.inList(searchItem.toLowerCase()))
        {
            System.out.println("Found " + searchItem);
        }
        else
        {
            System.out.println(searchItem + " not found");
        }
    }
    
    public static void processArrayList()
    {
        ArrayList<String> newArray = new ArrayList<>();
        //newArray.addAll(groceryList.getGroceryList());
        
        //String myArray[] = new String[](groceryList.getGroceryList().Size());
    }
}
