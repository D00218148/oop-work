/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.Arrays;
import java.util.Scanner;
/**
 *
 * @author D00218148
 */
public class arrays
{
    public static void main(String[] args)
    {
        //Scanner keyboard = new Scanner(System.in);
        int[] numbers = getIntegers();
        printArray(numbers);
        sortIntegers(numbers);
        
        
        
    }
    
    private static int[] getIntegers()
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter array size");
        int size = keyboard.nextInt();
        int[] arrayToSort = new int[size];
        
        for(int i = 0; i < arrayToSort.length; i++)
        {
            System.out.println("Enter number " + (1 + i));
            arrayToSort[i] = keyboard.nextInt();
        }
        return arrayToSort;
    }
    
    private static void printArray(int[] numbers)
    {
        if(numbers.length == 0)
        {
            System.out.println("stop");
        }
        else
        {
            System.out.print("[");
        }
        
        for(int i = 0; i < numbers.length; i++)
        {
            if(i == numbers.length - 1)
            {
                System.out.print(numbers[i] + "]");
            }
            else
            {
                System.out.print(numbers[i] + ", ");
            }
        }
        System.out.println();
    }
    
    private static void sortIntegers(int[] numbers)
    {
        int[] sortedArray = Arrays.copyOf(numbers, numbers.length);
        boolean continueFlag = false;
        int temp;
        while(continueFlag = false)
        {
            //continueFlag = false;
            for(int i = 0; i < sortedArray.length-1; i++)
            {
                if(sortedArray[i] < sortedArray[i+1])
                {
                    temp = sortedArray[i];
                    sortedArray[i] = sortedArray[i+1];
                    sortedArray[i+1] = temp;
                    continueFlag = true;
                }
            }
        }
        printArray(sortedArray);
    }
}
