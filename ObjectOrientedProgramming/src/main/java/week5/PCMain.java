package week5;

public class PCMain
{
    public static void main(String[] args)
    {
        Dimensions caseDimensions = new Dimensions(10, 40, 35);
        Case pcCase = new Case("P","Dell","240V","Plastic",1,caseDimensions);
        Motherboard pcMotherboard = new Motherboard("B210","Gigabyte",2,2,"B350",4,32,"Gigabyte bios");
        GPU pcGPU = new GPU("GTX 1080","Nvidia",4,8,true,5);
        Monitor pcMonitor = new Monitor("LG49","LG",49,new Resolution(5120,1140),60,"LCD");
        PC myPC = new PC(pcMotherboard, pcMonitor, pcCase, pcGPU);

        myPC.getPcCase().pressPowerButton();
        myPC.getMotherboard().loadProgram("Windows 10");
        myPC.getGpu().render(new MyImage("Windows Logo"));
        myPC.getMonitor().drawPixelAt(1500,1100,"pink");

        /*create a single room of a house using composition
        think about "has a" relationship of a room
        think about physical parts and furniture
         */

    }
}
