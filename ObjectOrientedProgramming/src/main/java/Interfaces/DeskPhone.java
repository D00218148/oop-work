package Interfaces;

public class DeskPhone implements ITelephone
{
    private int number;
    private boolean isRinging;

    public DeskPhone(int number) {
        this.number = number;
        this.isRinging = false;
    }

    @Override
    public void powerOn() {
        System.out.println("No action taken, desktop phones always on");
    }

    @Override
    public void dial(int phoneNumber) {
        System.out.println("Ringing " + phoneNumber);
    }

    @Override
    public void answer() {
        if(isRinging)
        {
            System.out.println("Answering phone");
            isRinging = false;
        }
    }

    @Override
    public boolean isRinging() {
        return isRinging;
    }

    public void setRinging(boolean ringing) {
        isRinging = ringing;
    }
}
