/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
/**
 *
 * @author Antanas Zalisauskas
 */
public class Contacts 
{
    private String name;
    private int phoneNum;
    
    public Contacts(String name, int number)
    {
        this.name = name;
        phoneNum = number;
    }
    
    public void changeName(String newName)
    {
        name = newName;
    }
    
    public void changeNumber(int newNumber)
    {
        phoneNum = newNumber;
    }
    
    public String printDetails()
    {
        String details = name + " " + phoneNum;
        return details;
    }
}
