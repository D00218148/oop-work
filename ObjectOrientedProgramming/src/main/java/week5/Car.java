/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;

import java.util.ArrayList;

/**
 *
 * @author D00218148
 */
public class Car extends Vehicle
{
    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private ArrayList<String> colour;
    
    private boolean isManual;
    private String currentGear;

    public Car(int doors, int wheels, String model, String engine, boolean isManual, String name, String size, String terrain, int maxSpeed, ArrayList<String> colour) {
        super(name, size, terrain, maxSpeed, colour);
        this.doors = doors;
        this.wheels = wheels;
        this.model = model;
        this.engine = engine;
        this.colour = colour;
        this.isManual = isManual;
        this.currentGear = "N";
    }

    
    /*
    public Car(int doors, int wheels, String model, String engine, String colour, boolean isManual) {
        this.doors = doors;
        this.wheels = wheels;
        this.model = model;
        this.engine = engine;
        this.colour = colour;
        this.isManual = isManual;
        this.currentGear = "N";
    }
    */
    
    public void changeGear(String newGear)
    {
        this.currentGear = newGear;
        System.out.println("Car.changeGear() called. Changed gear to " + this.currentGear);
    }
    
    public void changeVelocity(int speed, int direction)
    {
        System.out.println("Car.changeVelocity() called. Speed: " + speed + ", direction: " + direction);
        move(speed, direction);
    }
    
    public void setModel(String model)
    {
        String validModel = model.toLowerCase();
        if(validModel.equals("458") || validModel.equals("488 GTB"))
        {
            this.model = model;
        }
        else
        {
            this.model = "unknown";
        }
        
    }

    public String getCurrentGear() {
        return currentGear;
    }

    public String getModel()
    {
        return model;
    }
}
