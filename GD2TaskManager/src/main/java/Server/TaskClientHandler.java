package Server;

import Core.Task;
import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;
import java.util.Set;

public class TaskClientHandler implements Runnable
{
    private Socket clientSocket;
    private TaskManager taskList;
    //private Command command;

    public TaskClientHandler(Socket clientSocket, TaskManager taskList)
    {
        this.clientSocket = clientSocket;
        this.taskList = taskList;
    }

    @Override
    public void run()
    {
        try
        {
            //Open lines of communication
            //example of a decorator pattern
            Scanner clientInput = new Scanner(clientSocket.getInputStream());
            PrintWriter clientOutput = new PrintWriter(clientSocket.getOutputStream(), true);

            //set up repeated exchanges
            //look at what client sends and respond appropriately
            boolean sessionActive = true;
            while(sessionActive)
            {
                //protocol logic
                String request = clientInput.nextLine();

                //break up the request into components
                String[] components = request.split(TaskManagerServiceDetails.BREAKING_CHARACTERS);
                String response = null;
                Command command = CommandFactory.createCommand(components[0]);
                if(command != null)
                {
                    response = command.generateResponse(components, taskList);
                    if(response != null)
                    {
                        clientOutput.println(response);
                        if(command instanceof ExitCommand)
                        {
                            sessionActive = false;
                        }
                    }
                }
            }
            clientSocket.close();
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
