package com.dkit.gd2.antanaszalisauskas;

public class Staff implements Comparable<Staff>
//Comparable lets you sort values in an array by 'natural ordering'
//eg: sort by ID, sort by name, etc
//For this code to work, a comparable method needs to be implemented to sort arraylist by desired variable
{
    protected String id;
    protected String name;
    protected int bonus;
    protected String baseAirport;
    protected double baseAirport1;

    public Staff(String id, String name, int bonus, String baseAirport, double baseAirport1) {
        this.id = id;
        this.name = name;
        this.bonus = bonus;
        this.baseAirport = baseAirport;
        this.baseAirport1 = baseAirport1;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBonus() {
        return bonus;
    }

    public String getBaseAirport() {
        return baseAirport;
    }

    public double getBaseAirport1() {
        return baseAirport1;
    }

    @Override
    public String toString() {
        return "id=" + id + ", name=" + name + ", bonus=" + bonus + ", baseAirport=" + baseAirport + ", baseAirport1=" + baseAirport1;
    }

    @Override
    public int compareTo(Staff staff) {
        return this.id.compareTo(staff.id);
    }
}
