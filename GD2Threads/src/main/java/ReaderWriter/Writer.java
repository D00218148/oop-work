package ReaderWriter;

import java.util.Random;

public class Writer implements Runnable
{
    private Message message;

    public Writer(Message message)
    {
        this.message = message;
    }

    @Override
    public void run()
    {
        String messages[] = {
                "I like Java",
                "OOP is actually ok",
                "The last message is a lie",
                "We like javascript"
        };

        Random random = new Random();
        for(int i = 0; i < messages.length; i++)
        {
            message.write(messages[i]);
            try
            {
                Thread.sleep(random.nextInt(2000));
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }
        message.write("Finished");
    }
}
