package UnfairBankAccount;

public class MainApp
{
    public static void main(String[] args) {
        Account patrickAccount = new Account(1000);
        Runnable runner = new AccountThread(patrickAccount);

        Thread t1 = new Thread(runner);
        Thread t2 = new Thread(runner);
        Thread t3 = new Thread(runner);

        t1.start();
        t2.start();
        t3.start();

        //wait for the three threads to finish
        try
        {
            t1.join();
            t2.join();
            t3.join();
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println("Account balance is: " + patrickAccount.getBalance());

    }
}
