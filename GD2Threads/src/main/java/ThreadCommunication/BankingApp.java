package ThreadCommunication;

public class BankingApp
{
    public static void main(String[] args)
    {
        BankAccount account = new BankAccount();

        //create spender thread
        Spender spender = new Spender(account);
        Thread spenderThread = new Thread(spender);

        //create saver
        Saver saver = new Saver(account);
        Thread saverThread = new Thread(saver);

        spenderThread.start();
        saverThread.start();

        int time;
        if(args.length == 0)
        {
            time = 10000;
        }
        else
        {
            time = Integer.parseInt(args[0]) * 1000;
        }
        try
        {
            Thread.currentThread().sleep(time);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        account.close();
    }
}
