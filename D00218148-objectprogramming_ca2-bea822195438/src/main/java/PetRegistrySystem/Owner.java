package PetRegistrySystem;
//Author: Antanas Zalisauskas(D00218148)

public class Owner
{
    private String name;
    private String email;
    private String telephone;
    private String address;
    private String ownerID;


    public Owner(String name, String email, String telephone, String address, String ownerID)
    {
        this.name = name;
        this.email = email;
        this.telephone = telephone;
        this.address = address;
        this.ownerID = ownerID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getAddress() {
        return address;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /*
     * compareOwners, when called takes in an instance of Owner and compares
     * the current instance(this) to the other instance. If all variables are the same
     * the method returns true meaning both Owner instances are the same
     * @param temp - the other instance that is being compared to
     */
    public boolean compareOwners(Owner temp)
    {
        if((this.getName().equals(temp.getName())) && (this.getEmail().equals(temp.getEmail())) &&
                (this.getTelephone().equals(temp.getTelephone())) && (this.getAddress().equals(temp.getAddress())))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public String toString()
    {
        return this.getOwnerID() + " " + this.getName() + " " + this.getEmail() + " " + this.getTelephone() + " " + this.getAddress();
    }

    /*
     * checkID is used for whenever the user wants to edit owner details, or remove an owner.
     * This method takes in a String id which is compared to current instance of Owner.
     * If id matches, method returns true
     * @param id - user inputted id which is compared
     */
    public boolean checkID(String id)
    {
        if(this.getOwnerID().equals(id))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
