package SharedResources;

public class Resource
{
    private boolean conFree;

    public Resource()
    {
        conFree = true;
    }

    public synchronized void getConnection(String name)
    {
        System.out.println(name + " Entered getConnection()");
        while(conFree == false)
        {
            System.out.println(name + " Waiting to getConnection()");
            try
            {
                wait();
                System.out.println(name + " has been notified");
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getStackTrace());
            }
        }
        conFree = false;
        notifyAll();
    }

    public synchronized void freeConnection(String name)
    {
        System.out.println(name + " has entered freeConnection()");
        while(conFree == true)
        {
            try
            {
                wait();
            }
            catch(InterruptedException e)
            {
                System.out.println(e.getStackTrace());
            }
        }
        conFree = true;
        notifyAll();
    }
}
