package CountdownThread;

public class CountdownThread extends Thread
{
    private Countdown count;

    public CountdownThread(Countdown count)
    {
        this.count = count;
    }

    public void run()
    {
        count.doCountdown();
    }
}
