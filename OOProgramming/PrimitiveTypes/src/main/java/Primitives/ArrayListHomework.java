/*
create a program that implements a simple mobile phone.

the phone can store, modify, remove and query contact names.

need to create separate classes for Contact(name, phoneNumber).

also create a mobilePhone class which holds an ArrayList of contacts.

Add a menu of options that are available:
options: Quite, print list of contacts, add new contacts, update existing contacts,
remove contact and search contacts.

when adding or updating be sure to check if the contact already exists(use name)

be sure not to expose inner workings of the ArrayList to MobilePhone

MobilePhone should do everything with contact objects only

Contacts -> string name, String number
MobilePhone -> add, search, remove, edit
Main -> menu, inputs, call methods from MobilePhone
 */
package Primitives;

/**
 *
 * @author D00218148
 */
public class ArrayListHomework {
    
}
