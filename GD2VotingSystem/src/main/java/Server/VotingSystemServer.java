package Server;

import Core.VotingSystemServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VotingSystemServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        Map<String, String> loginDetails = new HashMap<>();
        Set<String> responses = new HashSet<>();

        populateLoginDetails(loginDetails);
        populateResponses(responses);

        try
        {
            listeningSocket = new ServerSocket(VotingSystemServiceDetails.LISTENING_PORT);

            //Set up a threadGroup to manage all clients
            ThreadGroup clientGroup = new ThreadGroup("Clients");
            clientGroup.setMaxPriority(Thread.currentThread().getPriority()-1);
            int threadCount = 0;

            while(true)
            {
                dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("the server has now accepted " + threadCount);

                
                //VotingSystemClientHandler clientHandler = new VotingSystemClientHandler(clientSocket);

                //Thread worker = new Thread(clientHandler);
                //worker.start();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    private static void populateLoginDetails(Map<String, String> loginDetails)
    {
        loginDetails.put("John", "Password");
        loginDetails.put("Marie", "SecretPassword");
    }

    private static void populateResponses(Set<String> responses)
    {

    }
}
