package BasicUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Receiver
{
    public static void main(String[] args)
    {
        int receiverPort = 50001;
        int senderPort = 50000;

        boolean continueRunning = true;

        final int MAX_LEN = 70;
        DatagramSocket receiverSocket = null;

        try
        {
            //create a datagram socket for receiving data
            receiverSocket = new DatagramSocket(receiverPort);

            System.out.println("Waiting for message on port " + receiverPort + "...");
            while(continueRunning)
            {
                byte buffer[] = new byte[MAX_LEN];

                DatagramPacket receivedData = new DatagramPacket(buffer, MAX_LEN);

                receiverSocket.receive(receivedData);

                //Convert the buffer of data to a string
                String message = new String(buffer);

                //display message
                System.out.println("Received message: " + message);

                //find out where the message came from
                InetAddress sender = receivedData.getAddress();
                System.out.println("Sender: " + sender);

                String responseMessage = message;

                byte[] responseArray = responseMessage.getBytes();

                //build packet and send it back

                DatagramPacket response = new DatagramPacket(responseArray, responseArray.length, sender, senderPort);
                receiverSocket.send(response);
            }
            Thread.sleep(8000);
        }
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(receiverSocket != null)
            {
                receiverSocket.close();
            }
        }
    }
}
