package com.dkit.gd2.antanaszalisauskas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

public class Theatre
{
    private int rows;
    private int seatsPerRow;
    private final String theatreName;
    public List<Seat> seats = new ArrayList<Seat>();

    public Theatre(String theatreName, int rows, int seatsPerRow) {
        this.rows = rows;
        this.seatsPerRow = seatsPerRow;
        this.theatreName = theatreName;
        int lastRow = 'A' + (rows - 1);
        for(char row = 'A'; row <= lastRow; row++)
        {
            for(int seatNum = 1; seatNum <= seatsPerRow; seatNum++)
            {
                double price = 12.00;
                if((row < 'D') && (seatNum >= 4 && seatNum <= 9))
                {
                    price = 14.00;
                }
                else if((row > 'F') || (seatNum < 4 || seatNum > 9))
                {
                    price = 7.00;
                }
                Seat seat = new Seat(row + String.format("%02d", seatNum), price);
                seats.add(seat);
            }
        }
    }

    public String getTheatreName() {
        return theatreName;
    }
    
    //binary search - worst case Log_2(numberSeats)
    //7 searches
    public boolean reserveSeatBS(String seatNumber)
    {
        int low = 0;
        int high = seats.size()-1;
        
        while(low <= high)
        {
            System.out.print(".");
            int mid = (low + high)/2;
            Seat midSeat = seats.get(mid);
            int cmp = midSeat.getSeatNumber().compareTo(seatNumber);
            
            if(cmp < 0)
            {
                low = mid + 1;
                
            }
            else if(cmp > 0)
            {
                high = mid - 1;
            }
            else
            {
                return seats.get(mid).reserve();
            }
        }
        System.out.println("There is no seat " + seatNumber);
        return false;
    }

    public boolean reserveSeat(String seatNumber)
    {
        Seat requestedSeat = null;
        for(Seat seat: seats)
        {
            System.out.println(".");
            if(seat.getSeatNumber().equals(seatNumber))
            {
                requestedSeat = seat;
                break;
            }
        }

        if(requestedSeat == null)
        {
            System.out.println("There is no seat " + seatNumber);
            return false;
        }

        return requestedSeat.reserve();
    }

    public void printSeats()
    {
        for(int i = 0; i < seats.size(); i++)
        {
            if(i % seatsPerRow == 0)
            {
                System.out.println();
            }
            System.out.print(seats.get(i).seatNumber + " ");
            
        }
        System.out.println();
    }

    public class Seat implements Comparable<Seat>
    {
        private final String seatNumber;
        private double price;
        private boolean reserved = false;

        public Seat(String seatNumber, double price)
        {
            this.seatNumber = seatNumber;
            this.price = price;
        }

        public int compareTo(Seat t) {
            return this.seatNumber.compareToIgnoreCase(t.getSeatNumber()); //To change body of generated methods, choose Tools | Templates.
        }
        
        

        public boolean reserve()
        {
            if(reserved)
            {
                return false;
            }
            else
            {
                this.reserved = true;
                System.out.println("Seat " + seatNumber + " reserved");
                return true;
            }
        }

        public boolean cancel()
        {
            if(!this.reserved)
            {
                return false;
            }
            else
            {
                this.reserved = false;
                System.out.println("Reservation of seat " + seatNumber + " cancelled");
                return true;
            }
        }

        public String getSeatNumber() {
            return seatNumber;
        }

        public double getPrice() {
            return price;
        }
    }
}
