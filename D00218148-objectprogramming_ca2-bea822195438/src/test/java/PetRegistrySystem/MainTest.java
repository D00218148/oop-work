/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PetRegistrySystem;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

/**
 *
 * @author D00218148
 */
public class MainTest {
    
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    
    public MainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
    }
    
    @After
    public void tearDown() {
    }
    
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    /**
     * Test of main method, of class Main.
     */
    @Test
    public void testAddNewPet() {
        systemInMock.provideLines("2","mammal","dog","barry","corgi","10","green and yellow","male","y");
        String[] args = null;
        Main.main(args);
        
        String welcome = "Welcome to Dundalk Pet Registry. Please enter your choice" + System.getProperty("line.separator");
        String menu1 = "Enter 0 to show available options" + System.getProperty("line.separator");
        String menu2 = "Enter 1 to add a new owner" + System.getProperty("line.separator");
        String menu3 = "Enter 2 to add a new pet" + System.getProperty("line.separator");
        String menu4 = "Enter 3 to quit" + System.getProperty("line.separator");
        String menu = menu1 + menu2 + menu3 + menu4;
        String exception = "Please don't enter CTRL+D as this input closes System.in meaning nothing else can be done with the program" + System.getProperty("line.separator");
        String addPet1 = "Please enter type of animal(mammal, bird, fish)" + System.getProperty("line.separator");
        String addPet2 = "Enter type of pet" + System.getProperty("line.separator");
        String addPet3 = "Enter name" + System.getProperty("line.separator");
        String addPet4 = "Enter breed" + System.getProperty("line.separator");
        String addPet5 = "Enter age" + System.getProperty("line.separator");
        String addPet6 = "Enter colour" + System.getProperty("line.separator");
        String addPet7 = "Enter gender" + System.getProperty("line.separator");
        String addPet8 = "Is pet neutered? Y/N" + System.getProperty("line.separator");
        String addPet9 = "Pet has been added" + System.getProperty("line.separator");
        String addPet = addPet1 + addPet2 + addPet3 + addPet4 + addPet5 + addPet6 + addPet7 + addPet8 + addPet9;
        String expOutput = welcome + menu + addPet + menu + exception;
        
        assertEquals(expOutput, outContent.toString());
    }
    
}
