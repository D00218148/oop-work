package com.dkit.gd2.antanaszalisauskas;

public class Series
{
    public long nSum(int n)
    {
        long total = 0;
        if(n > 0)
        {
            for(int i = 0; i <= n; i++)
            {
                total = total + n;
            }
            return total;
        }
        return 0;
    }

    public long factorial(int n)
    {
        long fact = 1;
        if(n >= 1)
        {
            for(int i = 1; i <= n; i++)
            {
                fact = fact*i;
            }
        }
        return fact;
    }

    public long fibonacci(int n)
    {
        if(n <= 1)
        {
            return 0;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }
}
