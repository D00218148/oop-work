package Tutorial;

import java.util.concurrent.locks.ReentrantLock;

public class BankAccount
{
    private double balance;
    private String accountNumber;
    private final ReentrantLock lock = new ReentrantLock();

    public BankAccount(double balance, String accountNumber)
    {
        this.balance = balance;
        this.accountNumber = accountNumber;
    }

    public void deposit(double amount)
    {
        lock.lock();
        try
        {
            this.balance += amount;
            System.out.println("deposited " + amount + ". New balance is: " + balance);
        }
        finally
        {
            lock.unlock();
        }
    }

    public void withdraw(double amount)
    {
        lock.lock();
        try
        {
            this.balance -= amount;
            System.out.println("withdrew " + amount + ". New balance is: " + balance);
        }
        finally
        {
            lock.unlock();
        }
    }

    public String getAccountNumber()
    {
        return this.accountNumber;
    }

    public void printAccountNumber()
    {
        System.out.println("Account number is: " + accountNumber);
    }
}
