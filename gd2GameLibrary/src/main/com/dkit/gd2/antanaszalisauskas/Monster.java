package src.main.com.dkit.gd2.antanaszalisauskas;

import java.util.ArrayList;
import java.util.List;

public class Monster implements ISavable
{
    private String name;
    private int hitpoints;
    private int strength;

    public Monster(String name, int hitpoints, int strength) {
        this.name = name;
        this.hitpoints = hitpoints;
        this.strength = strength;
    }

    public String getName() {
        return name;
    }

    public int getHitpoints() {
        return hitpoints;
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "name='" + name + '\'' +
                ", hitpoints=" + hitpoints +
                ", strength=" + strength +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monster)) return false;

        Monster monster = (Monster) o;

        if (getHitpoints() != monster.getHitpoints()) return false;
        if (getStrength() != monster.getStrength()) return false;
        return getName() != null ? getName().equals(monster.getName()) : monster.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + getHitpoints();
        result = 31 * result + getStrength();
        return result;
    }

    public List<String> write() {
        List<String> values = new ArrayList<String>();
        values.add(0, this.name);
        values.add(1, "" + this.getHitpoints());
        values.add(2, "" + this.getStrength());
        return values;
    }

    public void read(List<String> savedValues) {
        if(savedValues != null && savedValues.size() > 0)
        {
            this.name = savedValues.get(0);
            this.hitpoints = Integer.parseInt(savedValues.get(1));
            this.strength = Integer.parseInt(savedValues.get(2));
        }
    }

}
