/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Antanas Zalisauskas
 */
public class BookExercises61 
{
    public static void main(String[] args) 
    {
        Random rand = new Random();
        int[] numbers = new int[10];
        
        for(int i = 0; i < numbers.length; i++)
        {
            numbers[i] = rand.nextInt(100);
        }
        
        System.out.println("Array = " + Arrays.toString(numbers));
        
        evenIndex(numbers);
        evenElement(numbers);
        reverseOrder(numbers);
        firstAndLast(numbers);
    }
    
    private static void evenIndex(int[] numbers)
    {
        ArrayList<Integer> evenNumbers = new ArrayList<Integer>();
        
        for(int i = 0; i < numbers.length; i++)
        {
            if(i%2 == 0)
            {
                evenNumbers.add(numbers[i]);
            }
        }
        System.out.println("even numbers = " + evenNumbers.toString());
    }
    
    private static void evenElement(int[] numbers)
    {
        ArrayList<Integer> evenElements = new ArrayList<Integer>();
        evenElements.add(numbers[0]);
        
        for(int i = 0; i < numbers.length; i++)
        {
            if(numbers[i]%2 == 0)
            {
                evenElements.add(numbers[i]);
            }
        }
        System.out.println("even element = " + evenElements.toString());
    }
    
    private static void reverseOrder(int[] numbers)
    {
        int[] reverse = new int[10];
        int position = 0;
        for(int i = 9; i <= 0; i--)
        {
            reverse[position] = numbers[i];
            position++;
        }
        System.out.println(Arrays.toString(reverse));
    }
    
    private static void firstAndLast(int[] numbers)
    {
        System.out.println("First element = " + numbers[0] + ", Last element = " + numbers[numbers.length - 1]);
    }
}
