package Core;

public class ComboServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    //breaking characters
    public static final String COMMAND_SEPARATOR = "%%";

    //Command Strings
    public static final String END_SESSION = "QUIT";
    public static final String ECHO = "ECHO";
    public static final String DAYTIME = "DAYTIME";

    //Response
    public static final String UNRECOGNISED = "UNKNOWN COMMAND";
    public static final String SESSION_TERMINATED = "GOODBYE";
}
