/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week6;

/**
 *
 * @author D00218148
 */
public class StarWars extends Movie
{

    public StarWars() 
    {
        super("Star Wars");
    }

    @Override
    public String plot() 
    {
        return "Good guys fight bad guys in space";
    }
    
}
