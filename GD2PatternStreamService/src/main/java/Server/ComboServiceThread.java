package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ComboServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public ComboServiceThread(ThreadGroup group, String name, Socket dataSocket)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            input = new Scanner(new InputStreamReader((this.dataSocket.getInputStream())));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(ComboServiceDetails.END_SESSION))
            {
                response = null;
                incomingMessage = input.nextLine();
                System.out.println("Received message " + incomingMessage);

                String[] components = incomingMessage.split(ComboServiceDetails.COMMAND_SEPARATOR);
                CommandFactory factory = new CommandFactory();
                Command command = factory.createCommand(components[0]);

                if(command != null)
                {
                    response = command.createResponse(components);
                }
                else if(components[0].equals(ComboServiceDetails.END_SESSION))
                {
                    response = ComboServiceDetails.SESSION_TERMINATED;
                }
                else
                {
                    response = ComboServiceDetails.UNRECOGNISED;
                }

                //send response
                output.println(response);
            }
        }
        //need this for when client closes and input stream is corrupted
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }

    }
}
