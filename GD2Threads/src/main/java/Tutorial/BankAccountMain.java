package Tutorial;

public class BankAccountMain
{
    public static void main(String[] args)
    {
        BankAccount geralt = new BankAccount(0, "123ASD456");


        //anonymous thread
        Thread t1 = new Thread(){
            public void run(){
                //System.out.println("t1 called");
                geralt.deposit(300);
                geralt.withdraw(50);
                //System.out.println(geralt.getBalance());
            }
        };

        Thread t2 = new Thread(){
            public void run(){
                //System.out.println("t2 called");
                geralt.deposit(203.75);
                geralt.withdraw(100);
                //System.out.println(geralt.getBalance());
            }
        };

        //anonymous runnable
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                geralt.deposit(1);
                geralt.withdraw(2);
            }
        });

        t1.start();
        t2.start();


        //change synchronized to use reentrantLock
    }
}
