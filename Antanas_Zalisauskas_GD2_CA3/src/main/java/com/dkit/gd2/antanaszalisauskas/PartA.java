package com.dkit.gd2.antanaszalisauskas;

/**
 *
 * @author
 */
public class PartA
{

    public static void main(String[] args)
    {
        int nums[][] =
        {
            {
                2, 7, 6
            }, 
            {
                9, 5, 1
            },
            {
                4, 3, 8
            }
        }; 

        System.out.println("");
        if (isMagicSquare(nums))
        {
            System.out.println("\nData is in a magic square");
        }
        else
        {
            System.out.println("\nData is NOT in a magic square");
        }
        System.out.println("");
    }

    
    public static boolean isMagicSquare(int[][] nums)
    {
        if(nums.length > 0 && nums[0].length > 0)
        {
            int magicNumber = nums[0][0] + nums[0][1] + nums[0][2];
            for(int i = 0; i < nums.length; i++)
            {
                if((nums[i][0] + nums[i][1] + nums[i][2]) != magicNumber)
                {
                    return false;
                }
            }
            for(int i = 0; i < nums.length; i++)
            {
                if(nums[0][i] + nums[1][i] + nums[2][i] != magicNumber)
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

}
