package extendsThread;

public class MainApp
{
    public static void main(String[] args)
    {
        PrintChar printA = new PrintChar('A', 100);
        PrintChar printB = new PrintChar('B', 100);
        PrintNum print100 = new PrintNum(100);

        //you must start() for the thread to run
        print100.start();
        printA.start();
        printB.start();
    }
}
