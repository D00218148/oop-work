package week9;

import java.util.Comparator;

public class Employee implements Comparable<Employee>
{
    private int id;
    private String name;
    private int age;
    private String position;
    private long salary;



    public Employee(int id, String name, int age, String position, long salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.position = position;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getPosition() {
        return position;
    }

    public long getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int compareTo(Employee emp)
    {
        return (this.id - emp.id);
    }

    //Use comparator to sort employees by salary
    public static Comparator<Employee> SalaryComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee emp1, Employee emp2)
        {
            return (int)(emp1.getSalary() - emp2.getSalary());
        }
    };

    public static Comparator<Employee> AgeComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee emp1, Employee emp2)
        {
            return emp1.getAge() - emp2.getAge();
        }
    };

    public static Comparator<Employee> NameComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee emp1, Employee emp2)
        {
            return emp1.getName().compareTo(emp2.getName());
        }
    };
}
