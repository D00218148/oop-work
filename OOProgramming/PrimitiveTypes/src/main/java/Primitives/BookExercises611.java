package Primitives;

import java.util.Arrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Antanas Zalisauskas
 */
public class BookExercises611 
{
    public static void main(String[] args) 
    {
        int[] a = {1,4,9,16,9,7,4,9,11};
        int[] b = {11,1,4,9,16,9,7,4,9};
        
        System.out.println("Are arrays the same 6.11 = " + areArraysEqual(a, b));
        System.out.println("Are arrays equal 6.9 = " + equals(a, b));
        System.out.println("Sum of A(6.6) = " + alternatingSum(a));
    }
    
    private static boolean areArraysEqual(int[] a, int[] b)
    {
        boolean sameArrays = false;
        int[] copyA = Arrays.copyOf(a, a.length);
        int[] copyB = Arrays.copyOf(b, b.length);
        
        Arrays.sort(copyA);
        Arrays.sort(copyB);
        
        if(Arrays.equals(copyA, copyB))
        {
            sameArrays = true;
        }
        return sameArrays;
    }
    
    private static boolean equals(int[] a, int[] b)
    {
        boolean sameArrays = true;
        int i = 0;
        int[] copyA = Arrays.copyOf(a, a.length);
        int[] copyB = Arrays.copyOf(b, b.length);
        
        Arrays.sort(copyA);
        Arrays.sort(copyB);
        
        while(sameArrays == true && (i < copyA.length || i < copyB.length))
        {
            if(copyA[i] != copyB[i])
            {
                sameArrays = false;
            }
            i++;
        }
        return sameArrays;
    }
    
    private static int alternatingSum(int[] a)
    {
        int sum = 0;
        for(int i = 0; i < a.length; i++)
        {
            if(i%2 == 0)
            {
                sum += a[i];
            }
            else
            {
                sum -= a[i];
            }
        }
        return sum;
    }
}
