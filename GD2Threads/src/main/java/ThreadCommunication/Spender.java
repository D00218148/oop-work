package ThreadCommunication;

public class Spender implements Runnable
{
    private BankAccount account;

    public Spender(BankAccount ba)
    {
        this.account = ba;
    }

    @Override
    public void run()
    {
        while(account.isOpen())
        {
            if(account.withdraw(500))
            {
                System.out.println("€500 withdrawn");
                try
                {
                    Thread.currentThread().sleep(1000);
                }
                catch(InterruptedException e)
                {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
