package Client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class GenericTCPClient
{
    //take in input from user in the exact format expected of the server
    //ask user to enter the ip address and port of the server
    //connect to the server
    //use a scanner to get possible inputs
    //eg: add%%OOPCA6%%john%%1234567
    //remove%%OOPCA6
    //exit
    //viewAll
    //send the command to the server
    //get and print response
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        input.useDelimiter("\n");

        System.out.println("Please enter the hostname of the server: ");
        String hostname = input.next();
        System.out.println("Please enter the port number that the server is running on");
        int port = input.nextInt();

        try
        {
            Socket dataSocket = new Socket(hostname, port);

            Scanner serverIn = new Scanner(dataSocket.getInputStream());
            PrintWriter serverOut = new PrintWriter(dataSocket.getOutputStream(), true);

            boolean keepRunning = true;
            while(keepRunning)
            {
                System.out.println("Please enter the command to be sent");
                String toBeSent = input.next();

                //send to server
                serverOut.println(toBeSent);

                String response = serverIn.nextLine();
                System.out.println("Response: " + response);

                System.out.println("Do you want to keep running? (-1 to end or any other key to continue)");
                String choice = input.next();
                if(choice.equals("-1"))
                {
                    keepRunning = false;
                }
            }
            System.out.println("Thank you for using the task manager client");
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
