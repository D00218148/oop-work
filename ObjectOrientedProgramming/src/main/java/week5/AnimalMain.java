/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;

import java.util.ArrayList;

/**
 *
 * @author D00218148
 */
public class AnimalMain 
{
    public static void main(String[] args) 
    {
        /*
        Animal animal = new Animal("Bob", 5, "Domestic", false, 2, 2, 1, 12);
        animal.eat();
        animal.poo();
        animal.move();
        */
        
        ArrayList<String> harrysColours = new ArrayList<String>();
        harrysColours.add("Orange");
        harrysColours.add("white");
        Dog harry = new Dog(4, "Mongrel", true, 50, "Harry", 1, "Domestic", false, 1, 4, 0, 12, harrysColours);
        
        System.out.println(harry.toString());
        
        ArrayList<String> blobsColours = new ArrayList<String>();
        blobsColours.add("Green");
        blobsColours.add("White");
        Fish blob = new Fish(6, 2, 3, "Blob", 2, "Aquarium", false, 1, 1, 1, 1, blobsColours);
        
        /*challenge
        start with base class of vehicle, then create a Car that inherits the vehicle. Finally
        create another class which is a specific type of Car that inherits from the car class.
        
        you should be able to handle steering, changing gears and moving.
        you will need to decide where to put the appropiate state and behaviours.
        For the specific type of car add something particular to that type of car
        */
    }
    
    
}
