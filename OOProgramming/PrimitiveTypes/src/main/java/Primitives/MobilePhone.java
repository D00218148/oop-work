/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.ArrayList;
/**
 *
 * @author Antanas Zalisauskas
 */
public class MobilePhone 
{
    private ArrayList<Contacts> contactList = new ArrayList<Contacts>();
    
    public void addContact(String name, int number)
    {
        Contacts contact = new Contacts(name, number);
        contactList.add(contact);
        System.out.println("Contact added");
    }
    
    public void changeDetails(String oldName, String newName)
    {
        int position = findContact(oldName);
        if(position >= 0)
        {
            changeDetails(position, newName);
            System.out.println("Contact " + oldName + " has been changed to " + newName);
        }
    }
    
    public void changeDetails(int oldNum, int newNum)
    {
        int position = findContact(oldNum);
        if(position >= 0)
        {
            changeDetailsNum(position, newNum);
            System.out.println("Contact " + oldNum + " has been changed to " + newNum);
        }
    }
    
    private int findContact(String oldName)
    {
        return contactList.indexOf(oldName);
    }
    
    private int findContact(int oldNum)
    {
        return contactList.indexOf(oldNum);
    }
    
    private void changeDetails(int position, String newName)
    {
        contactList.get(position).changeName(newName);
    }
    
    private void changeDetailsNum(int position, int newNum)
    {
        contactList.get(position).changeNumber(newNum);
    }
    
    public boolean isContactThere(int number)
    {
        boolean contact = false;
        
        if(findContact(number) >= 0)
        {
            contact = true;
        }
        return contact;
    }
    
    public boolean isContactThere(String name)
    {
        boolean contact = false;
        
        if(findContact(name) >= 0)
        {
            contact = true;
        }
        return contact;
    }
    
    public void removeContact(String name)
    {
        int position = findContact(name);
        if(position >= 0)
        {
            contactList.remove(position);
            System.out.println("Contact removed");
        }
    }
    
    public void printAllContacts()
    {
        for(int i = 0; i < contactList.size(); i++)
        {
            System.out.println(contactList.get(i).printDetails());
        }
    }
}
