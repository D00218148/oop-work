/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daos;

import DTOs.User;
import Exceptions.DaoException;
import java.util.List;

/**
 * declares the methods that all userDAO types must implement
 * they could be mySQL, Mongo, Postgres
 * 
 * Classes in my Business Layer should use a reference to
 * the interface type to avoid dependencies on the underlying
 * concrete classes
 * @author D00218148
 */
public interface IUserDaoInterface 
{
    public List<User> findAllUsers() throws DaoException;
    public User findUserByUsernamePassword(String username, String password) throws DaoException;
    
}
