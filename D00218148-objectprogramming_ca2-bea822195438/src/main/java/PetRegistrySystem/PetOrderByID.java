package PetRegistrySystem;
//Author: Antanas Zalisauskas (D00218148)

import java.util.Comparator;

public class PetOrderByID implements Comparator<Pet>
{
    @Override
    public int compare(Pet pet1, Pet pet2)
    {
        return pet1.getPetID().compareTo(pet2.getPetID());
    }
}
