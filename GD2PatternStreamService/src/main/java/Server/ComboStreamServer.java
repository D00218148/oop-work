package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ComboStreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        ThreadGroup group = null;

        try
        {
            listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);

            group = new ThreadGroup("Client Threads");
            group.setMaxPriority(Thread.currentThread().getPriority() - 1);

            //main logic of server
            boolean continueRunning = true;
            //int threadCount = 0;
            while(continueRunning)
            {
                dataSocket = listeningSocket.accept();
                ComboServiceThread newClient = new ComboServiceThread(group, dataSocket.getInetAddress() + "", dataSocket);
                newClient.start();
                System.out.println("The server has now accepted " + group.activeCount() + " clients");
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                //will deal with data socket in thread class
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
    }
}
