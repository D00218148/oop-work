/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas.gd2solarsystem;

/**
 *
 * @author D00218148
 */
public class DogMain 
{
    public static void main(String[] args) 
    {
        Labrador rover = new Labrador("Rover");
        Dog rover2 = new Dog("Rover");
        
        //Labrador is an instance of dog but dog is not an instance of labrador
        System.out.println(rover2.equals(rover));
        System.out.println(rover.equals(rover2));
    }
}
