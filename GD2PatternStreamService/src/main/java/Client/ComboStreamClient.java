package Client;

import Core.ComboServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboStreamClient
{
    public static void main(String[] args)
    {
        Socket dataSocket = null;
        try
        {
            //connect to server
            dataSocket = new Socket("localhost", ComboServiceDetails.LISTENING_PORT);

            //build input output stream
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out), true);

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            //get user input
            Scanner keyboard = new Scanner(System.in);
            String message = "";
            while(!message.equals(ComboServiceDetails.END_SESSION))
            {
                displayMenu();
                int choice = getNumber(keyboard);
                String response = "";
                if(choice >= 0 && choice < 3)
                {
                    switch(choice)
                    {
                        case 0:
                            message = ComboServiceDetails.END_SESSION;
                            break;
                        case 1:
                            //ECHO%%The message to send
                            message = generateEcho(keyboard);
                            break;
                        case 2:
                            message = ComboServiceDetails.DAYTIME;
                            break;
                    }

                    //send to server
                    output.println(message); //no need to flush since autoflush enabled

                    //wait for response
                    response = input.nextLine();
                    System.out.println(response);
                }
                else
                {
                    System.out.println("Please enter a valid input");
                }
            }
            System.out.println("Thank you for using the combo service");
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException e)
            {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
    }

    public static void displayMenu()
    {
        System.out.println("0 to quit");
        System.out.println("1 to echo a message");
        System.out.println("2 to show daytime");
    }

    public static int getNumber(Scanner keyboard)
    {
        boolean numberEntered = false;
        int number = 0;
        while(!numberEntered)
        {
            try
            {
                number = keyboard.nextInt();
                numberEntered = true;
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return number;
    }

    public static String generateEcho(Scanner keyboard)
    {
        StringBuffer message = new StringBuffer(ComboServiceDetails.ECHO);
        message.append(ComboServiceDetails.COMMAND_SEPARATOR);
        System.out.println("What message would you like to echo");
        String echo = keyboard.nextLine();
        //add a language filter
        message.append(echo);
        return message.toString();
    }
}
