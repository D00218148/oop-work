package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ComboServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public ComboServiceThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);
        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run()
    {
        //loop
        //wait for message
        String incomingMessage = "";
        String response;
        //process message
        try
        {
            while(!incomingMessage.equals(ComboServiceDetails.END_SESSION))
            {
                response = null;
                incomingMessage = input.nextLine();
                System.out.println("recieved message: " + incomingMessage);

                String[] messageComponents = incomingMessage.split(ComboServiceDetails.COMMAND_SEPARATOR);
                if(messageComponents[0].equals(ComboServiceDetails.ECHO))
                {
                    StringBuffer echoMessage = new StringBuffer("");

                    if(messageComponents.length > 1)
                    {
                        echoMessage.append(messageComponents[1]);
                        for(int i = 2; i < messageComponents.length; i++)
                        {
                            echoMessage.append(ComboServiceDetails.COMMAND_SEPARATOR);
                            echoMessage.append(messageComponents[i]);
                        }
                    }

                    response = echoMessage.toString();
                }
                else if(messageComponents[0].equals(ComboServiceDetails.DAYTIME))
                {
                    response = new Date().toString();
                }
                else if(messageComponents[0].equals(ComboServiceDetails.END_SESSION))
                {
                    response = ComboServiceDetails.SESSION_TERMINATED;
                }
                else
                {
                    response = ComboServiceDetails.URECOGNISED;
                }

                //send response
                output.println(response);
                output.flush();
            }
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("Closing connection with client #" + number);
                dataSocket.close();
            }
            catch(IOException e)
            {
                System.out.println("Unable to disconnect + " + e.getMessage());
                System.exit(1);
            }
        }
        //TODO pair up with someone set up port forwarding on routers and test each others server to see if we connect
    }
}
