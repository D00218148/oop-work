package CountdownThread;

import static CountdownThread.ThreadColour.*;

public class Countdown
{
    private int i;
    public void doCountdown()
    {
        String colour;

        switch(Thread.currentThread().getName())
        {
            case "Thread 1":
                colour = ANSI_RED;
                break;
            case "Thread 2":
                colour = ANSI_GREEN;
                break;
            default:
                colour = ANSI_BLUE;
                break;
        }

        synchronized (this)
        {
            for(i = 10; i > 0; i--)
            {
                System.out.println(colour + Thread.currentThread().getName() + ": i = " + i);
            }
        }
    }
}
