package Server;

import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TaskManagerServer
{
    public static void main(String[] args)
    {
        TaskManager taskList = new TaskManager();
        ServerSocket listeningSocket = null;
        try
        {
            listeningSocket = new ServerSocket(TaskManagerServiceDetails.LISTENING_PORT);

            while(true)
            {
                Socket clientSocket = listeningSocket.accept();

                TaskClientHandler clientHandler = new TaskClientHandler(clientSocket, taskList);

                Thread worker = new Thread(clientHandler);
                worker.start();
            }
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(listeningSocket != null)
            {
                try
                {
                    listeningSocket.close();
                }
                catch(IOException e)
                {
                    System.out.println("Oopsie");
                    System.exit(1);
                }
            }
        }
    }
}
