/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week6;

/**
 *
 * @author D00218148
 */
public class MovieMain 
{
    public static void main(String[] args) 
    {
        for(int i = 0; i < 11; i++)
        {
            Movie movie = randomMovie();
            System.out.println("Movie #" + i + ": " + movie.getName() + "\n" + "Plot: " + movie.plot() + "\n");
        }
    }
    public static Movie randomMovie()
    {
        int randomNumber = (int)(Math.random()*5);
        System.out.println(randomNumber);
        switch(randomNumber)
        {
            case 0:
                return new LordOfTheRings();
            case 1:
                return new StarWars();
            case 2:
                return new IT();
            case 3:
                return new HarryPotter();
            case 4:
                return new Forgettable();
            default:
                return null;
        }
    }
}
