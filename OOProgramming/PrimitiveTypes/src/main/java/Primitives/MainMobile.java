/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;
import java.util.ArrayList;
import java.util.Scanner;
/**
 *
 * @author Antanas Zalisauskas
 */
public class MainMobile 
{
    private static Scanner keyboard = new Scanner(System.in);
    private static MobilePhone contactList = new MobilePhone();
    
    public static void main(String[] args) 
    {
        boolean quit = false;
        int option = 0;
        printOptions();
        
        while(quit == false)
        {
            System.out.println("Enter option");
            option = keyboard.nextInt();
            keyboard.nextLine();
            
            switch(option)
            {
                case 0:
                    printOptions();
                    break;
                case 1:
                    contactList.printAllContacts();
                    break;
                case 2:
                    insertContact();
                    break;
                case 3:
                    modifyContact();
                    break;
                case 4:
                    deleteContact();
                    break;
                case 5:
                    searchContact();
                    break;
                case 6:
                    quit = true;
                    break;
            }
        }
    }
    
    public static void printOptions()
    {
        System.out.println("Enter:");
        System.out.println("\t 0 - To show options");
        System.out.println("\t 1 - To show all contacts");
        System.out.println("\t 2 - To add new contact");
        System.out.println("\t 3 - To change a contacts details");
        System.out.println("\t 4 - To delete a contact");
        System.out.println("\t 5 - To search for existing contact");
        System.out.println("\t 6 - To close application");
    }
    
    public static void insertContact()
    {
        System.out.println("Enter name");
        String name = keyboard.nextLine();
        System.out.println("Enter number");
        int number = keyboard.nextInt();
        
        contactList.addContact(name.toLowerCase(), number);
    }
    
    public static void modifyContact()
    {
        System.out.println("Enter 1 To change name");
        System.out.println("Enter 2 to change number");
        int choice = keyboard.nextInt();
        
        if(choice == 1)
        {
            System.out.println("Enter old name");
            String oldName = keyboard.nextLine();
            keyboard.nextLine();
            System.out.println("Enter new name");
            String newName = keyboard.nextLine();
            contactList.changeDetails(oldName.toLowerCase(), newName.toLowerCase());
        }
        else if(choice == 2)
        {
            System.out.println("Enter old number");
            int oldNum = keyboard.nextInt();
            System.out.println("Enter new number");
            int newNum = keyboard.nextInt();
            contactList.changeDetails(oldNum, newNum);
        }
        else
        {
            
        }
    }
    
    public static void deleteContact()
    {
        System.out.println("Enter contact name");
        String name = keyboard.nextLine();
        
        contactList.removeContact(name);
    }
    
    public static void searchContact()
    {
        System.out.println("Enter 1 to search for name");
        System.out.println("Enter 2 to search for number");
        int choice = keyboard.nextInt();
        
        if(choice == 1)
        {
            System.out.println("Enter name");
            String name = keyboard.nextLine();
            boolean exists = contactList.isContactThere(name.toLowerCase());
            if(exists == true)
            {
                System.out.println("Contact already exists");
            }
            else
            {
                System.out.println("Contact not found");
            }
        }
        else if(choice == 2)
        {
            System.out.println("Enter number");
            int number = keyboard.nextInt();
            boolean exists = contactList.isContactThere(number);
            if(exists == true)
            {
                System.out.println("Contact already exists");
            }
            else
            {
                System.out.println("Contact not found");
            }
        }
        else
        {
            
        }
    }
}
