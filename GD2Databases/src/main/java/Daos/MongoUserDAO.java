package Daos;

import DTOs.User;
import Exceptions.DaoException;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

public class MongoUserDAO extends MongoDao implements IUserDaoInterface
{
    public List<User> findAllUsers() throws DaoException {
        MongoClient con = null;
        MongoDatabase db = null;
        List<User> users = new ArrayList<User>();

        try
        {
            con = this.getConnection();
            db = con.getDatabase("gd2_user");
            MongoCollection<Document> dbCollection = db.getCollection("user");
            FindIterable<Document> docs = dbCollection.find();
            for(Document doc: docs)
            {
                Double userIdDouble = (Double)doc.get("id");
                int userId = userIdDouble.intValue();
                String firstname = (String)doc.get("firstname");
                String lastname = (String)doc.get("lastname");
                String username = (String)doc.get("username");
                String password = (String)doc.get("password");

                User u = new User(userId, firstname, lastname, username, password);
                users.add(u);
            }
        }
        catch(MongoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ClassCastException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(con != null)
            {
                freeConnection(con);
            }
        }
        return users;
    }

    public User findUserByUsernamePassword(String username, String password) throws DaoException {
        User u = null;
        MongoClient con = null;

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("username", username);
        searchQuery.put("password", password);

        try
        {
            con = this.getConnection();
            MongoDatabase db = con.getDatabase("gd2_user");
            MongoCollection<Document> dbCollection = db.getCollection("user");
            FindIterable<Document> docs = dbCollection.find(searchQuery);

            for(Document doc: docs)
            {
                Double userIdDouble = (Double)doc.get("id");
                int userId = userIdDouble.intValue();
                String firstname = (String)doc.get("firstname");
                String lastname = (String)doc.get("lastname");
                String uname = (String)doc.get("username");
                String pword = (String)doc.get("password");

                u = new User(userId, firstname, lastname, username, password);
            }
        }
        catch(MongoException e)
        {
            System.out.println(e.getMessage());
        }
        catch(ClassCastException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(con != null)
            {
                freeConnection(con);
            }
        }
        return u;
    }
}
