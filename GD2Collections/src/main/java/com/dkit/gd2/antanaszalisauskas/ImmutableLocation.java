/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dkit.gd2.antanaszalisauskas;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author D00218148
 */
public final class ImmutableLocation 
{
    private final int locationID;
    private final String description;
    private final Map<String,Integer> exits;

    public ImmutableLocation(int locationID, String description, Map<String, Integer> exits) {
        this.locationID = locationID;
        this.description = description;
        if(exits != null)
        {
            this.exits = new HashMap<String, Integer>(exits);
        }
        else
        {
            this.exits = new HashMap<String, Integer>();
        }
    }

    public int getLocationID() {
        return locationID;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Integer> getExits() {
        return new HashMap<String, Integer>(exits);
    }
    
    
}
