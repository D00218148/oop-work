package Interfaces;

public class Main
{
    public static void main(String[] args)
    {
        ITelephone adamsPhone;

        adamsPhone = new DeskPhone(1234567);
        adamsPhone.powerOn();
        adamsPhone.dial(1234567);
        ((DeskPhone)adamsPhone).setRinging(true);
        adamsPhone.answer();

        ITelephone jacksPhone;
        jacksPhone = new MobilePhone(123456);
        jacksPhone.powerOn();
        jacksPhone.dial(34523);
        ((MobilePhone)jacksPhone).setRinging(true);
        jacksPhone.answer();
    }
}
