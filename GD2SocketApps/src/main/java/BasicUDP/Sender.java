package BasicUDP;

import java.io.IOException;
import java.net.*;
import java.rmi.UnknownHostException;

public class Sender
{
    public static void main(String[] args) {
        DatagramSocket senderSocket = null;
        DatagramPacket datagram = null;
        InetAddress receiverHost = null;
        try
        {

            receiverHost = Inet4Address.getByName("10.102.224.166");
        }
        catch(java.net.UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        int receiverPort = 50001;
        try
        {
            senderSocket = initializeSenderSocket(senderSocket, receiverHost, receiverPort);
            datagram = initializeDatagramPacket(datagram, receiverHost, receiverPort);

            //send message to the other program
            senderSocket.send(datagram);

            System.out.println("Message sent");

            getEchoResponseFromReceiver(senderSocket);

            Thread.sleep(8000);
        }
        catch(UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if(senderSocket != null)
            {
                senderSocket.close();
            }
        }
    }

    public static DatagramSocket initializeSenderSocket(DatagramSocket senderSocket, InetAddress receiverHost, int receiverPort)
    {
        try
        {
            //set up the address you want to send data to
            //receiverHost = Inet4Address.getLocalHost();

            //Create a port for the sender to listen on and receive into
            int senderPort = 50000;
            //receiverPort = 50001;

            //Create datagram socket for sending data
            //bind it to a port - 50000
            senderSocket = new DatagramSocket(senderPort);
        }
        /*
        catch(java.net.UnknownHostException e)
        {
            System.out.println(e.getMessage());
        }

         */
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        return senderSocket;
    }

    public static DatagramPacket initializeDatagramPacket(DatagramPacket datagram, InetAddress receiverHost, int receiverPort)
    {
        //Message
        String message = "Is your refrigerator running?(Antanas)";

        //get the information to be sent as a byte array
        byte buffer[] = message.getBytes();

        //build datagramPacket
        //Needs: buffer(data to be sent), buffer.length(how much data to send)
        //receiverHost(ip address of receiver), receiverPort(port the receiver is listening on)
        datagram = new DatagramPacket(buffer, buffer.length, receiverHost, receiverPort);
        return datagram;
    }

    public static void getEchoResponseFromReceiver(DatagramSocket senderSocket)
    {
        try
        {
            byte[] response = new byte[70];

            DatagramPacket echo = new DatagramPacket(response, response.length);

            //receive the response
            //the senderSocket is blocking - it will wait before it continues

            senderSocket.receive(echo);

            //get the contents of the response
            String responseMessage = new String(echo.getData());

            //display received data
            System.out.println("Response: " + responseMessage);
        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
