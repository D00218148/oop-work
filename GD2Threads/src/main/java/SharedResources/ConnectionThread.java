package SharedResources;

public class ConnectionThread implements Runnable
{
    private String name;
    private Resource resource;

    public ConnectionThread(String name, Resource resource)
    {
        this.name = name;
        this.resource = resource;
    }

    @Override
    public void run()
    {
        resource.getConnection(name);
        try
        {
            Thread.sleep(2000);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        resource.freeConnection(name);
    }
}
