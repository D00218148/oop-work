/*
-the purpose of this application is to help a company called Aidan's Burgers to manage their
process of selling their burgers.

-We want to help Aidan to select types of burgers, some additional items to be added
to be aded to burgers and pricing

-we want to create a base hamburger, but also other types of hamburger.

-the basic hamburger should have the following items:
Bread roll type, meat and up to 4 additions (lettuce, tomatoe, gherkin, carrot)
that the customer can select to add to the burger

-each of the additions get charged an additional price, so you need some way to 
track how many items got added abnd to calculate the final price (burger + additions).

-the base burger has a price and additions are all separately priced 

-create a hamburger class to deal with all of the above

-the constructor should only include the roll type, meat and price.
can also include name of the burger or you can use a setter

-also create two varieties of hamburgers(subclasses) to cater for:
1. healthy burger on brown rye bread roll plus two additional items(6 in total) 
that can be added
Hint: you probably want to process the two additional items in the new class, not the base
class, since 2 additional items only relate to the new class
2. Deluxe hamburger - comes with chips and drinks as additions, but no extra additions
allowed
Hint: you have to find a way to automatically add these additions when the deluxe burger
is being created and prevent other additions

-all three classes should have a method that can be called anytime to show
the base price of the hamburger plus all additions, each showing the addition name
and addition price and a grand total for the burger(base price + all additions)

-for the two additional classes this may require you to look at the base class for pricing
and then adding totals to the final price
 */
package readingWeek;

/**
 *
 * @author D00218148
 */
public class Hamburgers {
    
}
