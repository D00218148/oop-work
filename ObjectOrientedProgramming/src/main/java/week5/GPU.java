package week5;

public class GPU
{
    private  String model;
    private String manufacturer;
    private int cudaCores;
    private int vRam;
    private boolean fourKSupport;
    private int output;

    public GPU(String model, String manufacturer, int cudaCores, int vRam, boolean fourKSupport, int output) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.cudaCores = cudaCores;
        this.vRam = vRam;
        this.fourKSupport = fourKSupport;
        this.output = output;
    }

    public void render(MyImage image)
    {
        System.out.println("Rendering the image " + image.getName());
    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getCudaCores() {
        return cudaCores;
    }

    public int getvRam() {
        return vRam;
    }

    public boolean isFourKSupport() {
        return fourKSupport;
    }

    public int getOutput() {
        return output;
    }
}
