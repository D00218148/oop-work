
package com.dkit.gd2.antanaszalisauskas;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 *
 * @author 
 */
public class Flight
{
    private String id;
    private String origin;
    private String destination;
    private ArrayList<Staff> staffList;

    public Flight(String id, String origin, String destination)
    {
        this.id = id;
        this.origin = origin;
        this.destination = destination;
        this.staffList = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }

    public boolean addStaff(Staff staffToAdd)
    {
        if(staffToAdd == null)
        {
            System.out.println("Staff details cannot be empty");
            return false;
        }
        if(staffList.size() == 0)
        {
            System.out.println("Staff added");
            staffList.add(staffToAdd);
            return true;
        }
        for(Staff staff: staffList)
        {
            if(staff.getId().equals(staffToAdd.getId()))
            {
                System.out.println("Staff ID already exists");
                return false;
            }
        }
        System.out.println("Staff added");
        staffList.add(staffToAdd);
        return true;
    }

    public void displayStaff()
    {
        for(Staff staff: staffList)
        {
            System.out.println(staff.toString());
        }
    }

    public void displayNamesWithFlightHours()
    {
        for(Staff staff: staffList)
        {
            System.out.print("name=" + staff.getName());
            if(staff instanceof Contract)
            {
                System.out.print(", flight hours=" + ((Contract)staff).getNumberOfFlightHours());
            }
            System.out.println();
        }
    }

    public void sortStaff()
    {
        Collections.sort(staffList);
    }

    public void sortStaffByNameWithinAirport()
    {
        System.out.println("Sorting staff....");
        staffList.sort(staffNameComparator);
    }

    private static Comparator<Staff> staffNameComparator = new Comparator<Staff>() {
        @Override
        public int compare(Staff staff1, Staff staff2) {
            return staff1.name.compareTo(staff2.name);
        }
    };

    //Polymorphism is when you cast an object to a different parameter
    //Example of polymorphism can be seen in lines 117 and 121
    public void updateAllStaffBonus()
    {
        for(Staff staff: staffList)
        {
            if(staff instanceof Permanent)
            {
                ((Permanent)staff).updateBonus(this);
            }
            else
            {
                ((Contract)staff).updateBonus(this);
            }
        }
    }

    public void readStaffFromFile(String fileName)
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader(fileName))))
        {
            while(scanner.hasNextLine())
            {
                String input = scanner.nextLine();
                String[] data = input.split(",");
                String type = data[0];
                String id = data[1];
                String name = data[2];
                int bonus = Integer.parseInt(data[3]);
                String baseAirport = data[4];
                double baseAirport1 = Double.parseDouble(data[5]);
                if(type.equals("P"))
                {
                    int overnightAllowance = Integer.parseInt(data[5]);
                    Permanent staff = new Permanent(id, name, bonus, baseAirport, baseAirport1, overnightAllowance);
                    this.addStaff(staff);
                }
                else
                {
                    int topUpBonus = Integer.parseInt(data[5]);
                    int numberOfFlightHours = Integer.parseInt(data[6]);
                    Contract staff = new Contract(id, name, bonus, baseAirport, baseAirport1, topUpBonus, numberOfFlightHours);
                    this.addStaff(staff);
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }

    }
}
