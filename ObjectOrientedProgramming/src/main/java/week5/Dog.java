/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;

import java.util.ArrayList;

/**
 *
 * @author D00218148
 */
public class Dog extends Animal 
{
    private int legs;
    private String breed;
    private boolean tail;
    private int teeth;

    public Dog(int legs, String breed, boolean tail, int teeth, String name, int age, String habitat, boolean wildOrTame, int size, int weight, int dangerLevel, int averageLifespan, ArrayList<String> colour) {
        super(name, age, habitat, wildOrTame, size, weight, dangerLevel, averageLifespan, colour);
        this.legs = legs;
        this.breed = breed;
        this.tail = tail;
        this.teeth = teeth;
    }
    
    private void chew()
    {
        System.out.println("Dog.chew() called");
    }
    
    @Override
    public void eat()
    {
        System.out.println("Dog.eat() called");
        chew();
        super.eat();
    }

    @Override
    public String toString() {
        return "Dog{" + "legs=" + legs + ", breed=" + breed + ", tail=" + tail + ", teeth=" + teeth + super.toString() + '}';
    }
    
    
}
