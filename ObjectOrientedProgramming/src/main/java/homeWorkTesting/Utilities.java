/*
challenge 1 - create a test class for each method that fails
challenge 2 - write 2 pairs for removePairs() method
              (AABCDDEF -> ABCDEF, ABCCABDEEF -> ABCABDEF)
 */
package homeWorkTesting;

/**
 *
 * @author D00218148
 */
public class Utilities 
{
    /* Returns a char array containing every nth char.
    when sourceArray.length < n, return sourceArray
    */
    
    public char[] everyNthChar(char[] sourceArray, int n)
    {
        if(sourceArray == null || sourceArray.length < n)
        {
            return sourceArray;
        }
        
        int returnLength = sourceArray.length/n;
        char[] result = new char[returnLength];
        int index = 0;
        
        for(int i = n-1; i < sourceArray.length; i+=n)
        {
            result[index++] = sourceArray[i];
        }
        return result;
    }
    
    /*Removes pairs of the same character that are next to each other
    by removing one occurrence of the character
    EX: AABBCCDDFF -> ABCDF
        ABCDEEF -> ABCDEF
    */
    
    public String removePairs(String source)
    {
        //if the length < 2 there can't be any pairs
        if(source == null || source.length() < 2)
        {
            return source;
        }
        
        StringBuilder sb = new StringBuilder();
        char [] string = source.toCharArray();
        for(int i = 0; i < string.length - 1; i++)
        {
            if(string[i] != string[i+1])
            {
                sb.append(string[i]);
            }
        }
        sb.append(string[string.length - 1]);
        return sb.toString();
    }
    
    /*Conversion based on internal rules
    */
    public int converter(int a, int b)
    {
        return (a/b) + (a*30) - 2;
    }
    
    public String nullIfOddLength(String source)
    {
        if(source.length()%2 == 0)
        {
            return source;
        }
        return null;
    }
}
