package ThreadGroups;

public class PiggyBankApp
{
    private PiggyBank bank = new PiggyBank();
    private Thread thread[] = new Thread[100];

    public PiggyBankApp()
    {
        ThreadGroup g1 = new ThreadGroup("group");
        boolean done = false;

        for(int i = 0; i < 100; i++)
        {
            thread[i] = new Thread(g1, new AddAPennyThread(), "t");
            thread[i].start();
        }

        while(!done)
        {
            if(g1.activeCount() == 0)
            {
                done = true;
            }
        }
    }

    private static synchronized void addAPenny(PiggyBank bank)
    {
        double newBalance = bank.getBalance() + 0.01;
        try
        {
            Thread.sleep(5);
        }
        catch(InterruptedException e)
        {
            System.out.println(e.getMessage());
        }
        bank.setBalance(newBalance);
    }

    public class AddAPennyThread extends Thread
    {
        @Override
        public void run()
        {
            addAPenny(bank);
        }
    }

    public static void main(String[] args)
    {
        PiggyBankApp test = new PiggyBankApp();
        System.out.println("Balance: " + String.format("%.2f", test.bank.getBalance()));
    }
}
