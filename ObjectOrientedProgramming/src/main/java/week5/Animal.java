/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week5;
import java.util.ArrayList;
/**
 *
 * @author D00218148
 */
public class Animal 
{
    private String name;
    private int age;
    private String habitat;
    private boolean wild;
    private int size;
    private int weight;
    private int dangerLevel;
    private int averageLifespan;
    private ArrayList<String> colour;
    
    public Animal(String name, int age, String habitat, boolean wild, int size, int weight, int dangerLevel, int averageLifespan, ArrayList<String> colour) {
        this.name = name;
        this.age = age;
        this.habitat = habitat;
        this.wild = wild;
        this.size = size;
        this.weight = weight;
        this.dangerLevel = dangerLevel;
        this.averageLifespan = averageLifespan;
        this.colour = colour;
    }
    
    public void eat()
    {
        System.out.println("Animal.eat() called");
    }
    
    public void poo()
    {
        System.out.println("Animal.poo() called");
    }
    
    public void move(int speed)
    {
        System.out.println("Animal.move() called. Animal is moving at " + speed + " speed");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getHabitat() {
        return habitat;
    }

    public boolean isWild() {
        return wild;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public int getDangerLevel() {
        return dangerLevel;
    }

    public int getAverageLifespan() {
        return averageLifespan;
    }

    public ArrayList<String> getColour() {
        return colour;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", age=" + age + ", habitat=" + habitat + ", wild=" + wild + ", size=" + size + ", weight=" + weight + ", dangerLevel=" + dangerLevel + ", averageLifespan=" + averageLifespan + ", colour=" + colour + '}';
    }
    
    
}
