package Core;

public class ComboServiceDetails
{
    public static final int LISTENING_PORT = 50000;
    public static final String COMMAND_SEPARATOR = "%%";

    //command strings
    public static final String END_SESSION = "QUIT";
    public static final String ECHO = "ECHO";
    public static final String DAYTIME = "DAYTIME";

    //response strings
    public static final String URECOGNISED = "UNKNOWN COMMAND";
    public static final String SESSION_TERMINATED = "GOODBYE";
}
