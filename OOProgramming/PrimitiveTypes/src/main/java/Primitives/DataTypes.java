/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Primitives;

/**
 *
 * @author D00218148
 */
public class DataTypes 
{
    public static void main(String[] args) 
    {
        playWithInteger();
        bitShiftTesting();
        showJavaUsesIntAsDefault();
        playWithFloats();
        playWithDouble();
        playWithChars();
        extractBitsFromByte();
        generalBitsFromByte();
    }
    
    public static void playWithInteger()
    {
        System.out.println("An integer is " + Integer.SIZE + " Bits");
        int maxInt = Integer.MAX_VALUE;
        int minInt = Integer.MIN_VALUE;
        System.out.println("Max size " + maxInt + ", min size " + minInt);
    }
    
    public static void bitShiftTesting()
    {
        int one = 1;
        int oneTimesSeven = 1 << 7;
        System.out.println(oneTimesSeven);
    }
    
    public static void extractBitsFromByte()
    {
        byte soundsInGame = 43;
        checkSirenSound(soundsInGame);
    }
    
    private static void checkSirenSound(byte soundsInGame)
    {
        byte sirenChecker = 1;
        final int sirenBitPosition = 3;
        sirenChecker = (byte)(sirenChecker << sirenBitPosition);
        byte andResult = (byte)(sirenChecker & soundsInGame);
        int onOrOff = andResult >> sirenBitPosition;
        System.out.println(onOrOff);
    }
    
    public static void generalBitsFromByte()
    {
        byte sounds = 43;
        int bitPosition = 3;
        generalCheck(sounds, bitPosition);
    }
    
    private static void generalCheck(byte sounds, int position)
    {
        byte checker = 1;
        checker = (byte)(checker << position);
        byte result = (byte)(checker & sounds);
        int onOrOff = result >> position;
        System.out.println(onOrOff);
    }
    
    public static void showJavaUsesIntAsDefault()
    {
        byte smallNumber = 100;
        byte otherSmallNumber = (byte)(smallNumber/2);
    }
    
    public static void playWithFloats()
    {
        System.out.println("A float is " + Float.SIZE + " Bits");
        float maxFloat = Float.MAX_VALUE;
        float minFloat = Float.MIN_VALUE;
        System.out.println("Max size " + maxFloat + ", min size " + minFloat);
        float averageGD2Mark = 52.678f;
    }
    
    public static void playWithDouble()
    {
        System.out.println("A double is " + Double.SIZE + " Bits");
        double maxDouble = Double.MAX_VALUE;
        double minDouble = Double.MIN_VALUE;
        System.out.println("Max size " + maxDouble + ", min size " + minDouble);
        double averageGD2Mark = 52.678;
    }
    
    public static void playWithChars()
    {
        char input = 5;
        System.out.println(input);
    }
}
