package com.dkit.gd2.antanaszalisauskas;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public class MainApp
{
    public static void main(String[] args)
    {
        ObjectMapper objectMapper = new ObjectMapper();

        Car car = new Car("convertible", 3, 3, "Boxster", "Porsche", 140, "Diesel", 3.0, 4, "Orange");

        //writeCarJsonToFile(objectMapper, car);
        writeCarJsonToString(objectMapper, car);
        //createCarFromJsonString(objectMapper);
        //TestJsonNode(objectMapper);
        //createListFromJSONArrayString(objectMapper);
        //deserializeJSONStringExtraFields(objectMapper);
        //useCustomSerializer();
        //useCustomDeserializer();
        //testDateFormatter();
        //deserializeToArray();
    }

    public static void deserializeToArray()
    {
        String jsonCarArray = "[{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}, {\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Cayman\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}]";

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
        try
        {
            Car[] cars = mapper.readValue(jsonCarArray, Car[].class);
            for(Car car: cars)
            {
                System.out.println(car);
            }
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static void testDateFormatter()
    {
        ObjectMapper mapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        mapper.setDateFormat(df);
        Car car = new Car("convertible", 3, 3, "Boxster", "Porsche", 140, "Diesel", 3.0, 4, "Orange");

        Request request = new Request(car);

        try
        {
            String requestAsString = mapper.writeValueAsString(request);
            System.out.println(requestAsString);
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void useCustomDeserializer()
    {
        String json = "{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}";

        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomCarDeserializer", new Version(1,0,0,null,null,null));
        module.addDeserializer(Car.class, new CustomCarDeserializer());
        mapper.registerModule(module);

        try
        {
            Car car = mapper.readValue(json, Car.class);
            System.out.println(car);
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static void useCustomSerializer()
    {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule("CustomCarSerializer", new Version(1,0,0,null,null,null));
        module.addSerializer(Car.class, new CustomCarSerializer());
        mapper.registerModule(module);

        Car car = new Car("convertible", 3, 3, "Boxster", "Porsche", 140, "Diesel", 3.0, 4, "Orange");
        try
        {
            String carJson = mapper.writeValueAsString(car);
            System.out.println("Custom version: " + carJson);
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
    }

    public static void deserializeJSONStringExtraFields(ObjectMapper mapper)
    {
        String json = "{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\", \"bootsize\" : \"Medium\"}";
        try
        {
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Car car = mapper.readValue(json, Car.class);
            System.out.println("Deserailized car: " + car);


            JsonNode jsonNodeRoot = mapper.readTree(json);
            JsonNode jsonNodeBootsize = jsonNodeRoot.get("bootsize");
            String bootSize = jsonNodeBootsize.asText();
            System.out.println(bootSize);
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static void printMap(Map<String, Object> cMap)
    {
        for(Map.Entry<String, Object> entry : cMap.entrySet())
        {
            System.out.println(entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public static void createMapFromJSONString(ObjectMapper mapper)
    {
        String json = "{\"colour\" : \"Black\", \"type\" : \"BMW\"}";
        try
        {
            Map<String, Object> carMap = mapper.readValue(json, new TypeReference<Map<String, Object>>() {});
            printMap(carMap);
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static void printList(List<Car> carList)
    {
        for(Car car: carList)
        {
            System.out.println(car.toString());
        }
    }

    public static void createListFromJSONArrayString(ObjectMapper mapper)
    {
        String jsonCarArray = "[{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}, {\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Cayman\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}]";
        try
        {
            List<Car> carList = mapper.readValue(jsonCarArray, new TypeReference<List<Car>>() {});
            printList(carList);
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static void TestJsonNode(ObjectMapper objectMapper)
    {
        String json = "{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}\n";
        try
        {
            JsonNode jsonNode = objectMapper.readTree(json);
            String colour = jsonNode.get("colour").asText();
            System.out.println("Colour: " + colour);
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }

    public static Car createCarFromJsonString(ObjectMapper objectMapper)
    {
        Car car = null;
        String json = "{\"body\":\"convertible\",\"wheels\":3,\"doors\":3,\"model\":\"Boxster\",\"make\":\"Porsche\",\"topSpeed\":140,\"fuelType\":\"Diesel\",\"engineSize\":3.0,\"noughtToSixty\":4,\"colour\":\"Orange\"}\n";
        try
        {
            car = objectMapper.readValue(json, Car.class);
            System.out.println(car);
        }
        catch(JsonMappingException e)
        {
            System.out.println(e.getMessage());
        }
        catch(JsonProcessingException e)
        {
            System.out.println(e.getMessage());
        }
        return car;
    }

    public static void writeCarJsonToFile(ObjectMapper objectMapper, Car car)
    {
        try
        {
            objectMapper.writeValue(new File("car.json"), car);
        }
        catch(JsonGenerationException e)
        {
            e.getMessage();
        }
        catch(JsonMappingException e)
        {
            e.getMessage();
        }
        catch(IOException e)
        {
            e.getMessage();
        }
    }

    public static void writeCarJsonToString(ObjectMapper objectMapper, Car car)
    {
        try
        {
            String carAsString = objectMapper.writeValueAsString(car);
            System.out.println(carAsString);
        }
        catch(JsonProcessingException e)
        {
            e.getMessage();
        }
    }
}
